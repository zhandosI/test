import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

import { User } from "@app/shared/models/user";
import { LoginFormError } from "@app/shared/models/errors/login-form.error";

@Component({
    selector: 'app-login-form',
    templateUrl: './login-form.component.html',
    styleUrls: ['./login-form.component.scss']
})
export class LoginFormComponent implements OnInit {

    user: User;
    error: LoginFormError;
    @Output() submitCredentials: EventEmitter<User> = new EventEmitter();
    @Input() loading: boolean;

    constructor() { }

    ngOnInit() {
        this.user = new User();
        this.error = new LoginFormError();
    }

    onSubmit = (): void => {
        this.error.username = '';
        this.error.password = '';

        if (this.isValid(this.user)) {
            this.submitCredentials.emit(this.user);
        }

    }

    isValid = (user: User): boolean => {
        if (!user.username) { this.error.username = 'Имя пользователя не может быть пустым!'; }
        if (!user.password) { this.error.password = 'Пароль не может быть пустым!'; }

        return !this.error.username && !this.error.password;

    }

}
