import {Component} from '@angular/core';
import {Router} from "@angular/router";
import {AuthService} from "@app/shared/services/auth.service";
import {User} from "@app/shared/models/user";

@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.scss']
})
export class LoginComponent {
    loading = false;
    globalErrorMsg: string;

    constructor(private authService: AuthService,
                private router: Router,) { }

    auth = (user: User) => {
        this.loading = true;
        this.globalErrorMsg = '';

        this.authService.auth(user).subscribe(
            () => {
                this.loading = false;
                this.router.navigate(['/']);
        }, errRes => { 
                if (errRes.status === 401) {
                    this.globalErrorMsg = errRes.error.message;
                }
                this.loading = false;
            }
        );
    }

}
