import { Component, OnInit } from '@angular/core';
import * as $ from 'jquery';
import {UsersService} from '@app/shared/services/users/users.service';

@Component({
    selector: 'app-sidebar',
    templateUrl: './sidebar.component.html',
    styleUrls: ['./sidebar.component.scss']
})
export class SidebarComponent implements OnInit {

    loggedInUserRole: string;

    showMenu: string = '';

    constructor(private userService: UsersService) {}

    ngOnInit() {
        this.loggedInUserRole = this.userService.getLoggedInUserRole();

        console.log("DSSDA", this.loggedInUserRole)

        $(document).ready(function() {
            $("#sidenavToggler").click(function(e) {
                e.preventDefault();
                $("body").toggleClass("sidenav-toggled");
            });
        })
    }

    addExpandClass(element: any) {
        if (element === this.showMenu) {
            this.showMenu = '0';
        } else {
            this.showMenu = element;
        }
    }

}
