import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AdminComponent } from './admin.component';

const routes: Routes = [
  {
    path: '',
    component: AdminComponent,
    children: [
        { path: '', loadChildren: './modules/dashboard/dashboard.module#DashboardModule' },
        { path: 'catalogs', loadChildren: './modules/catalogs/catalogs.module#CatalogsModule' },
        { path: 'calculations', loadChildren: './modules/calculations/calculations.module#CalculationsModule' },
        { path: 'users', loadChildren: './modules/users/users.module#UsersModule' },
        { path: 'enterprises', loadChildren: './modules/enterprises/enterprises.module#EnterprisesModule' }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdminRoutingModule { }
