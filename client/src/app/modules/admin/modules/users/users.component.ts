import { Component, OnInit } from '@angular/core';
import {routerTransition} from "@app/router.animations";
import {NgbModal} from "@ng-bootstrap/ng-bootstrap";
import {Role} from "@app/shared/models/role";
import {UsersService} from "@app/shared/services/users/users.service";
import * as $ from "jquery";
import {User} from "@app/shared/models/user";
@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.scss'],
  animations: [routerTransition()]
})
export class UsersComponent implements OnInit {

  roles: Role[];
  users: User[];

  tabs: string[] = ['users','roles','actions'];
  selectedTab = this.tabs[0];
  modalReference: any;
  loading: boolean = false;
  role: Role;
  user: User;
  addRoleDataModel: any = {};


  constructor(private modalService: NgbModal,
              private userService: UsersService) { }


  ngOnInit() {
    this.role  = new Role();
    this.user = new User();
    this.user.role = this.role;
    this.fetchData();
  }

  newModal(addNew) {

    this.modalReference = this.modalService.open(addNew, { centered: true });
    this.modalReference.result.then((result) => {
      console.log(result);
    }, (reason) => {
      console.log(reason);
    });
  }

  addNewRoleData() {
    this.loading = true;

    this.userService.addRole(this.role)
        .subscribe(() => {
          this.addRoleDataModel = {};
          this.modalReference.close();
          this.fetchData();
        }, error => {
          this.addRoleDataModel = {};
          console.log(error);
        });
  }

  addNewUserData() {
    this.loading = true;

    this.userService.addUser(this.user)
        .subscribe(() => {
          this.modalReference.close();
          this.fetchData();
        }, error => {
          this.addRoleDataModel = {};
          console.log(error);
        });
  }


  private fetchData() {
    this.loading = true;
    this.userService.getAllRoles().subscribe((roles) => {
      this.roles = roles;
    })

    this.userService.getAllUsers().subscribe((users: User[]) => {

      this.users = users.filter(u => u.username !== this.userService.getLoggedInUsername());

      $(document).ready(() => ($("#dataTableInfo") as any).DataTable({
        "pageLength": 10
      }));
      this.loading = false;
    })
  }




}
