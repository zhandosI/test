import {Component, Input, OnInit} from '@angular/core';
import { User } from "@app/shared/models/user";
import * as $ from "jquery";
import {routerTransition} from "@app/router.animations";

@Component({
  selector: 'app-info',
  templateUrl: './info.component.html',
  styleUrls: ['./info.component.scss'],
  animations: [routerTransition()]
})
export class InfoComponent implements OnInit {

  loading: boolean = false;
  @Input()
  users: User[];


  constructor() { }

  ngOnInit() {
    $(document).ready(() => ($("#dataTableRoles") as any).DataTable({
      "pageLength": 10
    }));
  }


}
