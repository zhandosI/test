import {Component, Input, OnInit} from '@angular/core';
import * as $ from "jquery";

import {Role} from "@app/shared/models/role";
import {routerTransition} from "@app/router.animations";

@Component({
  selector: 'app-roles',
  templateUrl: './roles.component.html',
  styleUrls: ['./roles.component.scss'],
  animations: [routerTransition()]
})
export class RolesComponent implements OnInit {

  loading: boolean = false;
  @Input() roles: Role[];

  constructor() { }

  ngOnInit() {
    $(document).ready(() => ($("#dataTableRoles") as any).DataTable({
      "pageLength": 10
    }));
  }

}
