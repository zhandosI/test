import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { UsersRoutingModule } from './users-routing.module';
import {UsersComponent} from "@app/modules/admin/modules/users/users.component";
import {DataService} from "@app/shared/services/data.service";
import {SharedModule} from "@app/shared/shared.module";
import {FormsModule} from "@angular/forms";
import {UsersService} from "@app/shared/services/users/users.service";
import { InfoComponent } from './info/info.component';
import { ActionsComponent } from './actions/actions.component';
import { RolesComponent } from './roles/roles.component';
import { NgbDropdownModule} from '@ng-bootstrap/ng-bootstrap';

@NgModule({
  imports: [
    CommonModule,
    UsersRoutingModule,
    SharedModule,
    FormsModule,
    NgbDropdownModule.forRoot(),
  ],
  providers: [UsersService, DataService],
  declarations: [UsersComponent, InfoComponent, ActionsComponent, RolesComponent]
})
export class UsersModule { }
