import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';
import { AuthService } from '@app/shared/services/auth.service';
import {HttpErrorResponse} from "@angular/common/http";
import {Observable} from "rxjs";
import "rxjs-compat/add/observable/of";

@Injectable()
export class AuthGuard implements CanActivate {

    constructor(private router: Router,
                private authService: AuthService) {}

    canActivate = () => {
        if (localStorage.getItem('api_token')) {
            return this.authService.isValidToken(localStorage.getItem('api_token'))
                .map(isValid => {
                    if (!isValid) {
                        localStorage.removeItem('api_token');
                        this.router.navigate(['/auth/login']);
                    }
                    return isValid;
                }).catch((error: HttpErrorResponse) => {
                    localStorage.removeItem('api_token');
                    this.router.navigate(['/auth/login']);
                    return Observable.of(error);
                });
        } else {
            this.router.navigate(['/auth/login']);
            return false;
        }
    }
}
