import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import { LoaderLightComponent } from './components/loaders/loader-light/loader-light.component';
import { LoaderDarkMaterialComponent } from './components/loaders/loader-dark-material/loader-dark-material.component';
import { LoaderImageComponent } from './components/loaders/loader-image/loader-image.component';
import { SuccessMessageComponent } from './components/messages/success-message/success-message.component';
import { WarningMessageComponent } from './components/messages/warning-message/warning-message.component';
import { InfoMessageComponent } from './components/messages/info-message/info-message.component';
import { ErrorMessageComponent } from './components/messages/error-message/error-message.component';
import { PageHeaderComponent } from './components/page-header/page-header.component';
import { DefaultSnackbarComponent } from './components/snackbars/default-snackbar/default-snackbar.component';
import { PaginatorComponent } from '@app/shared/components/paginator/paginator.component';
import { MaterialModule } from '@app/shared/material.module';
import { RouterModule } from '@angular/router';
import { TypeaheadModule } from 'ngx-bootstrap';

@NgModule({
    imports: [
        CommonModule,
        MaterialModule,
        RouterModule,
        TypeaheadModule.forRoot(),
    ],
    declarations: [
        LoaderLightComponent,
        LoaderDarkMaterialComponent,
        LoaderImageComponent,
        SuccessMessageComponent,
        InfoMessageComponent,
        ErrorMessageComponent,
        WarningMessageComponent,
        PageHeaderComponent,
        DefaultSnackbarComponent,
        PaginatorComponent
    ],
    exports: [
        LoaderLightComponent,
        LoaderDarkMaterialComponent,
        LoaderImageComponent,
        SuccessMessageComponent,
        InfoMessageComponent,
        ErrorMessageComponent,
        WarningMessageComponent,
        PageHeaderComponent,
        DefaultSnackbarComponent,
        PaginatorComponent
    ]
})
export class SharedModule {
}
