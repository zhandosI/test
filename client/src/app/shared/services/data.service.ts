import { Injectable } from '@angular/core';

@Injectable()
export class DataService {

    private _successEdit: string;
    private _successDelete: string;
    private _errorMsg: string;

constructor() {}

    get successAdd() { return this._successEdit; }
    set successAdd(val: string) { this._successEdit = val; }

    get successEdit() { return this._successEdit; }
    set successEdit(val: string) { this._successEdit = val; }

    get successDelete() { return this._successDelete; }
    set successDelete(val: string) { this._successDelete = val; }

    get errorMsg() { return this._errorMsg; }
    set errorMsg(val: string) { this._errorMsg = val; }

}