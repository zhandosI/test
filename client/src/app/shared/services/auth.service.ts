import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { AuthResponse } from '../models/auth.response';

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';
import {User} from "@app/shared/models/user";

@Injectable()
export class AuthService {

    private readonly apiUrl = '/api/v1/auth';

    constructor(private http: HttpClient) {}


    auth = (user: User): Observable<any> =>
            this.http.post(`${this.apiUrl}`,user).map((res: AuthResponse) =>{
                if (res.status === 'success') {
                    localStorage.setItem('api_token', res.data.token);
                }

                return res;
            }).catch(this.handleError)

    logout = (): Observable<any> =>
        Observable.create(obs => {
            localStorage.removeItem('api_token');
            obs.next();
        })

    isValidToken = (token: string): Observable<any> =>
        this.http.post(`${this.apiUrl}/token`, { token }).map((res: AuthResponse) => {
            return res.status === 'success' && res.data.isValid === true;
        }).catch(this.handleError)

    private handleError = (error: HttpErrorResponse) => Observable.throw(error);

}
