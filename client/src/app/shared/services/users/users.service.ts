import {Injectable} from "@angular/core";
import {HttpClient, HttpErrorResponse} from "@angular/common/http";
import {Observable} from "rxjs";
import {ApiResponse} from "@app/shared/models/api.response";

// @ts-ignore
import jwt_decode from 'jwt-decode';

@Injectable()
export class UsersService {

    private readonly usersApiUrl = '/api/v1/users';
    private readonly rolesApiUrl = '/api/v1/roles';

    constructor(private http: HttpClient) {}

    getLoggedInUserId = (): number => {
        const decoded = jwt_decode(localStorage.getItem('api_token'));
        return decoded.userId;
    }

    getLoggedInUsername = (): string => {
        const decoded = jwt_decode(localStorage.getItem('api_token'));
        return decoded.sub;
    }

    getLoggedInUserRole = (): string => {
        const decoded = jwt_decode(localStorage.getItem('api_token'));
        return decoded.role;
    }

    getAllUsers = (): Observable<any> =>
        this.http.get(this.usersApiUrl)
            .map((res: ApiResponse) => {
                if (res.status === 'success')
                    return res.data;
            })
            .catch(this.handleError);


    getAllRoles = (): Observable<any> =>
        this.http.get(this.rolesApiUrl)
            .map((res: ApiResponse) => {
                if (res.status === 'success')
                    return res.data;
            })
            .catch(this.handleError);

    addUser = (userData:any): Observable<any> =>
        this.http.post(this.usersApiUrl,userData)
            .map((res: ApiResponse) => {
                if (res.status === 'success')
                    return res.data;
        })
            .catch(this.handleError);

    addRole = (roleData: any): Observable<any> =>
        this.http.post(this.rolesApiUrl,roleData)
            .map((res: ApiResponse) => {
                if (res.status === 'success')
                    return res.data;
            })
            .catch(this.handleError);

    private handleError = (error: HttpErrorResponse) => Observable.throw(error);

}
