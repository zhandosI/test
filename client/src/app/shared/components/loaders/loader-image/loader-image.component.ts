import { Component, Input } from '@angular/core';

@Component({
  selector: 'loader-image',
  templateUrl: './loader-image.component.html',
  styleUrls: ['./loader-image.component.scss']
})
export class LoaderImageComponent {

  @Input() label: string;
  @Input() class: string;

}
