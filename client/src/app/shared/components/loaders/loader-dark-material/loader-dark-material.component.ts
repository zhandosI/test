import { Component, Input } from '@angular/core';

@Component({
    selector: 'app-loader-dark-material',
    templateUrl: './loader-dark-material.component.html',
    styleUrls: ['./loader-dark-material.component.scss']
})
export class LoaderDarkMaterialComponent {

    @Input() label: string;
    @Input() class: string;

}
