import {
  Component,
  OnInit,
  Input,
  ChangeDetectionStrategy,
  EventEmitter,
  OnChanges
} from '@angular/core';

export class PageEvent {
  /** The current page index. */
  pageIndex: number;
  /** The current page size */
  limit: number;
}

@Component({
  selector: 'paginator',
  templateUrl: './paginator.component.html',
  styleUrls: ['./paginator.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class PaginatorComponent implements OnInit, OnChanges {
  @Input()
  total: number;
  @Input()
  limit = 20;
  @Input()
  pageIndex = 1;
  @Input()
  pageSizeOptions: number[];
  pager: any;
  readonly page = new EventEmitter<PageEvent>();
  constructor() {}
  ngOnChanges() {
    this.pager = this.getPager(this.total, this.pageIndex, this.limit, false);
  }
  ngOnInit() {}

  setPage(page) {
    this.pageIndex = page;
    this.pager = this.getPager(this.total, this.pageIndex, this.limit, true);
  }

  setSize(size) {
    this.limit = size;
    this.pageIndex = 1;
    this.pager = this.getPager(this.total, this.pageIndex, this.limit, true);
  }

  emitEvent(index: number, size: number) {
    // emit event
    const pageEvent = new PageEvent();
    pageEvent.pageIndex = index;
    pageEvent.limit = size;
    this.page.emit(pageEvent);
  }

  getPager(totalItems: number, pageIndex: number = 1, limit: number = 10, emitEvent: boolean) {
    // calculate total pages
    const totalPages = Math.ceil(totalItems / limit);

    // ensure current page isn't out of range
    if (pageIndex < 1) {
      pageIndex = 1;
    } else if (pageIndex > totalPages) {
      pageIndex = totalPages;
    }

    let startPage: number, endPage: number;
    if (totalPages <= 10) {
      // less than 10 total pages so show all
      startPage = 1;
      endPage = totalPages;
    } else {
      // more than 10 total pages so calculate start and end pages
      if (pageIndex <= 6) {
        startPage = 1;
        endPage = 10;
      } else if (pageIndex + 4 >= totalPages) {
        startPage = totalPages - 9;
        endPage = totalPages;
      } else {
        startPage = pageIndex - 5;
        endPage = pageIndex + 4;
      }
    }

    // calculate start and end item indexes
    const startIndex = (pageIndex - 1) * limit;
    const endIndex = Math.min(startIndex + limit - 1, totalItems - 1);

    // create an array of pages to ng-repeat in the pager control
    const pages = Array.from(Array(endPage + 1 - startPage).keys()).map(
      i => startPage + i
    );

    if (emitEvent) {
      this.emitEvent(pageIndex, limit);
    }

    // return object with all pager properties required by the view
    return {
      totalItems: totalItems,
      pageIndex: pageIndex,
      limit: limit,
      totalPages: totalPages,
      startPage: startPage,
      endPage: endPage,
      startIndex: startIndex,
      endIndex: endIndex,
      pages: pages
    };
  }
}
