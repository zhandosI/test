export class FormError {

    public name: string;
    public shortName: string;
    public bin: string;
    public registrationNumber: string;
    public enterpriseCode: string;
    public size: string;
    public projectAvailability: string;
    public briefCharacteristic: string;
    public productionObjectDangerClass: string;
    public number: string;
    public pollutionSourcesType: string;
    public emittingSourceNumber: string;
    public dailyWorkingHours: string;
    public yearlyWorkingHours: string;

    constructor() {

    }

}
