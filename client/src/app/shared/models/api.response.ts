export class ApiResponse {

    constructor(public status: string,
                public code: string,
                public message: string,
                public links: any,
                public data: any) { }
}
