import {Role} from "@app/shared/models/role";

export class User {

    public id: number;
    public username: string;
    public password: string;
    public firstname: string;
    public lastname: string;
    public middlename: string;
    public role: Role;

    constructor () { }

}
