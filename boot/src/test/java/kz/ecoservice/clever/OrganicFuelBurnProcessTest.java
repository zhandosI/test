package kz.ecoservice.clever;

import org.camunda.bpm.engine.test.Deployment;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@Deployment(resources = { "bpmn/organic_fuel_burn_process.bpmn" })
public class OrganicFuelBurnProcessTest {

    @Test
    public void process_init_test() {
    }

}
