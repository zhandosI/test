package kz.ecoservice.clever;

import org.camunda.bpm.engine.ProcessEngine;
import org.camunda.bpm.engine.RuntimeService;
import org.camunda.bpm.engine.test.Deployment;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@Deployment(resources = { "bpmn/dummy_process.bpmn" })
public class ProcessApplicationIT {

    @Autowired
    public ProcessEngine processEngine;

    @Test
    public void processDeployed() {
        RuntimeService runtimeService = processEngine.getRuntimeService();
        runtimeService.startProcessInstanceByKey("dummy_process");
        runtimeService.createProcessInstanceQuery().list();
    }

}
