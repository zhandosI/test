package kz.ecoservice.clever.controller.rest;

import kz.ecoservice.clever.controller.BaseController;
import kz.ecoservice.clever.model.dto.RoleDto;
import kz.ecoservice.clever.service.RoleService;
import kz.ecoservice.clever.util.mapper.impl.RoleMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/v1/roles")
public class RoleRestController extends BaseController {

    private final RoleService roleService;
    private final RoleMapper roleMapper;

    @Autowired
    public RoleRestController(RoleService roleService, RoleMapper roleMapper) {
        this.roleService = roleService;
        this.roleMapper = roleMapper;
    }

    @GetMapping(value = "", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<?> getAll() {
        return success()
                .withStatus(HttpStatus.OK)
                .withBody(roleMapper.toDto(roleService.findAll()))
                .build();

    }

    @PostMapping(value = "", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<?> saveRole(@RequestBody RoleDto roleDto) {
        return success()
                .withStatus(HttpStatus.OK)
                .withBody(roleService.save(roleMapper.toEntity(roleDto)))
                .build();
    }
}
