package kz.ecoservice.clever.controller.rest;

import kz.ecoservice.clever.controller.BaseController;
import kz.ecoservice.clever.model.dto.UserDto;
import kz.ecoservice.clever.model.entity.User;
import kz.ecoservice.clever.service.UserService;
import kz.ecoservice.clever.util.mapper.impl.UserMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

@RestController
@RequestMapping("/api/v1/users")
public class UserRestController extends BaseController {

    private final UserService userService;
    private final UserMapper userMapper;

    @Autowired
    public UserRestController(UserService userService,
                              UserMapper userMapper) {
        this.userService = userService;
        this.userMapper = userMapper;
    }

    @GetMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<?> getUserById(@PathVariable("id") Long id) {
        return success()
                .withStatus(HttpStatus.OK)
                .withBody(userMapper.toDto(userService.findOne(id)))
                .build();
    }

    @GetMapping(value = "", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<?> getAllUsers() {
        return success()
                .withStatus(HttpStatus.OK)
                .withBody(userMapper.toDto(userService.findAll()))
                .build();
    }

    @PostMapping(value = "", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<?> saveUser(@RequestBody UserDto user) {
        return success()
                .withStatus(HttpStatus.OK)
                .withBody(userService.save(userMapper.toEntity(user)))
                .build();
    }

}
