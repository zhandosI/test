package kz.ecoservice.clever.controller;

import kz.ecoservice.clever.model.ErrorCode;
import kz.ecoservice.clever.model.exception.ApplicationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestController;

@ControllerAdvice
@RestController
public class ErrorController extends BaseController {

    @ExceptionHandler(ApplicationException.class)
    public ResponseEntity<?> applicationException(ApplicationException e) {
        HttpStatus status;

        if (e.getErrorCode() == ErrorCode.INTERNAL_ERROR)
            status = HttpStatus.INTERNAL_SERVER_ERROR;
        else if (e.getErrorCode() == ErrorCode.AUTH_ERROR)
            status = HttpStatus.UNAUTHORIZED;
        else if (e.getErrorCode() == ErrorCode.RESOURCE_NOT_FOUND)
            status = HttpStatus.NOT_FOUND;
        else
            status = HttpStatus.INTERNAL_SERVER_ERROR;

        return error()
                .withStatus(status)
                .withException(e)
                .build();
    }

}
