package kz.ecoservice.clever.config;

import org.apache.http.HttpHost;
import org.apache.http.client.HttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.DefaultUriBuilderFactory;

@Configuration
@ComponentScan("kz.ecoservice.clever")
@EnableAutoConfiguration
public class WebConfig {

    @Bean
    public RestTemplate calcRestClient() {
        HttpHost calcHost = new HttpHost("localhost", 8080);
        HttpClient httpClient = HttpClientBuilder.create().build();
        HttpComponentsClientHttpRequestFactory factory = new HttpComponentsClientHttpRequestFactory(httpClient);

        RestTemplate restTemplate = new RestTemplate(factory);
        restTemplate.setRequestFactory(factory);
        restTemplate.setUriTemplateHandler(new DefaultUriBuilderFactory(calcHost.toURI()));

        return restTemplate;
    }

}
