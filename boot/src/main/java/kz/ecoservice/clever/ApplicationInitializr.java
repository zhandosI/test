package kz.ecoservice.clever;

import lombok.extern.slf4j.Slf4j;
import org.camunda.bpm.spring.boot.starter.annotation.EnableProcessApplication;
import org.camunda.bpm.spring.boot.starter.event.PostDeployEvent;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.event.EventListener;

@SpringBootApplication
@EnableProcessApplication
@Slf4j
public class ApplicationInitializr extends SpringBootServletInitializer {

    public static void main(String... args) {
        SpringApplication.run(ApplicationInitializr.class);
    }

    @EventListener
    public void postDeploy(PostDeployEvent event) {
        log.info("CAMUNDA POST DEPLOY EVENT: {}", event);
    }

}
