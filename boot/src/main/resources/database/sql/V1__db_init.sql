create database clever_core_db;
create user clever_core_db_admin with encrypted password 'P@ssw0rd';
grant all privileges on database clever_core_db to clever_core_db_admin;

CREATE TABLE public.user_sessions
(
    id bigserial PRIMARY KEY NOT NULL,
    start_date timestamp DEFAULT current_timestamp NOT NULL,
    end_date timestamp,
    active boolean DEFAULT true  NOT NULL
);
CREATE UNIQUE INDEX user_sessions_id_uindex ON public.user_sessions (id);