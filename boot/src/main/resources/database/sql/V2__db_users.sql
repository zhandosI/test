drop table if exists user_sessions;
drop table if exists clever_users;
drop table if exists clever_roles;

CREATE TABLE public.clever_roles
(
    id bigserial PRIMARY KEY NOT NULL,
    code varchar(255) NOT NULL,
    name varchar(255) NOT NULL,
    created_at timestamp DEFAULT current_timestamp NOT NULL,
    modified_at timestamp DEFAULT current_timestamp NOT NULL,
    status varchar(25) DEFAULT 'ACTIVE' NOT NULL
);

CREATE UNIQUE INDEX clever_roles_id_uindex ON public.clever_roles (id);
CREATE UNIQUE INDEX clever_roles_code_uindex ON public.clever_roles (code);

CREATE TABLE public.clever_users
(
    id bigserial PRIMARY KEY NOT NULL,
    username varchar(255) NOT NULL,
    password varchar(255) NOT NULL,
    firstname varchar(255),
    lastname varchar(255),
    middlename varchar(255),
    created_at timestamp DEFAULT current_timestamp NOT NULL,
    modified_at timestamp DEFAULT current_timestamp NOT NULL,
    status varchar(25) DEFAULT 'ACTIVE' NOT NULL
);
CREATE UNIQUE INDEX clever_users_id_uindex ON public.clever_users (id);
CREATE UNIQUE INDEX clever_users_username_uindex ON public.clever_users (username);

ALTER TABLE public.clever_users ADD role_id bigint NOT NULL;

ALTER TABLE public.clever_users
ADD CONSTRAINT clever_users_clever_roles_id_fk
FOREIGN KEY (role_id) REFERENCES public.clever_roles (id);

