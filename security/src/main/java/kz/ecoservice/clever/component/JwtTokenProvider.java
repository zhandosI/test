package kz.ecoservice.clever.component;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Clock;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.impl.DefaultClock;
import kz.ecoservice.clever.model.dto.JwtTokenDto;
import kz.ecoservice.clever.model.entity.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.servlet.http.HttpServletRequest;
import java.util.Base64;
import java.util.Date;
import java.util.function.Function;

@Component
public class JwtTokenProvider {

    private final Clock clock = DefaultClock.INSTANCE;

    @Value("${security.jwt.token.secret}")
    private String secret;

    @Value("${security.jwt.token.expiration}")
    private long expirationMillis;

    private UserDetailsService userDetailsService;

    @Autowired
    @Qualifier("jwtUserDetailsService")
    public void setUserDetailsService(UserDetailsService userDetailsService) {
        this.userDetailsService = userDetailsService;
    }

    @PostConstruct
    protected void init() {
        secret = Base64.getEncoder().encodeToString(secret.getBytes());
    }

    public JwtTokenDto generateToken(User user) {
        final Date createdDate = clock.now();
        final Date expirationDate = calculateExpirationDate(createdDate);
        String token = this.generateClaims(user, createdDate, expirationDate);

        return new JwtTokenDto(token, createdDate, expirationMillis, true);
    }

    public String getTokenFromReq(HttpServletRequest req) {
        final String bearer = "Bearer ";
        String headerStr = req.getHeader("Authorization");

        if (headerStr != null && headerStr.startsWith(bearer))
            return headerStr.substring(bearer.length()).trim();

        return null;
    }

    public String getTokenFromReq(String authHeader) {
        final String bearer = "Bearer ";

        if (authHeader != null && authHeader.startsWith(bearer))
            return authHeader.substring(bearer.length()).trim();

        return null;
    }

    public Authentication getAuthentication(String token) {
        UserDetails userDetails = userDetailsService.loadUserByUsername(getUsername(token));
        return new UsernamePasswordAuthenticationToken(userDetails, "", userDetails.getAuthorities());
    }

    public boolean validateToken(String token) {
        // Additional expiration logic could be added
        return !isTokenExpired(token);
    }

    public Long getUserIdFromReq(String authHeader) {
        String token = getTokenFromReq(authHeader);
        return getUserId(token);
    }

    private Long getUserId(String token) {
        final Claims claims = getAllClaims(token);
        return Long.valueOf((String) claims.get("userId"));
    }

    private String getUsername(String token) {
        return getClaim(token, Claims::getSubject);
    }

    private String generateClaims(User user, Date issuedAt, Date expiresAt) {
        Claims claims = Jwts.claims()
                .setSubject(user.getUsername())
                .setIssuedAt(issuedAt)
                .setExpiration(expiresAt);

        claims.put("userId", String.valueOf(user.getId()));
        claims.put("role", user.getRole().getCode());

        return Jwts.builder()
                .setClaims(claims)
                .signWith(SignatureAlgorithm.HS256, secret)
                .compact();
    }

    private <T> T getClaim(String token, Function<Claims, T> claimsResolver) {
        final Claims claims = getAllClaims(token);
        return claimsResolver.apply(claims);
    }

    private Claims getAllClaims(String token) {
        return Jwts.parser()
                .setSigningKey(secret)
                .parseClaimsJws(token)
                .getBody();
    }

    private Date getExpirationDate(String token) {
        return getClaim(token, Claims::getExpiration);
    }

    private Date calculateExpirationDate(Date createdDate) {
        return new Date(createdDate.getTime() + expirationMillis * 4);
    }

    private Boolean isTokenExpired(String token) {
        final Date expiration = getExpirationDate(token);
        return expiration.before(clock.now());
    }

}
