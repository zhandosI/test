package kz.ecoservice.clever.service.impl;

import kz.ecoservice.clever.model.JwtUser;
import kz.ecoservice.clever.model.JwtUserFactory;
import kz.ecoservice.clever.model.entity.User;
import kz.ecoservice.clever.service.UserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service("jwtUserDetailsService")
@Slf4j
public class JwtUserDetailsServiceImpl implements UserDetailsService {

    private final UserService userService;

    @Autowired
    public JwtUserDetailsServiceImpl(UserService userService) {
        this.userService = userService;
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        User user = userService.findOne(username);

        if (user == null)
            throw new UsernameNotFoundException("User with username " + username + " not found.");

        JwtUser jwtUser = JwtUserFactory.create(user);
        log.info("IN loadUserByUsername: User with username - {} successfully loaded", username);
        return jwtUser;
    }
}
