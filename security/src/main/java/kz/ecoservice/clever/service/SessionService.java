package kz.ecoservice.clever.service;

import kz.ecoservice.clever.model.entity.Session;

import java.util.List;

public interface SessionService {

    List<Session> findAll();

}
