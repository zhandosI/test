package kz.ecoservice.clever.service;


import kz.ecoservice.clever.model.UserCredentials;
import kz.ecoservice.clever.model.dto.JwtTokenDto;

public interface SecurityService {

    JwtTokenDto auth(UserCredentials credentials);
    JwtTokenDto validateToken(String token);

}
