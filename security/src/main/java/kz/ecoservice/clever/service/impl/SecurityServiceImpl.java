package kz.ecoservice.clever.service.impl;

import kz.ecoservice.clever.component.JwtTokenProvider;
import kz.ecoservice.clever.model.UserCredentials;
import kz.ecoservice.clever.model.dto.JwtTokenDto;
import kz.ecoservice.clever.model.entity.User;
import kz.ecoservice.clever.model.exception.ApplicationException;
import kz.ecoservice.clever.model.exception.builder.ApplicationExceptionBuilder;
import kz.ecoservice.clever.model.exception.builder.AuthExceptionBuilder;
import kz.ecoservice.clever.model.exception.builder.ExceptionDirector;
import kz.ecoservice.clever.service.SecurityService;
import kz.ecoservice.clever.service.UserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.Objects;

@Service
@Slf4j
public class SecurityServiceImpl implements SecurityService {

    private final AuthenticationManager authenticationManager;
    private final UserService userService;
    private final JwtTokenProvider provider;


    @Autowired
    public SecurityServiceImpl(AuthenticationManager authenticationManager,
                               UserService userService,
                               JwtTokenProvider provider) {
        this.authenticationManager = authenticationManager;
        this.userService = userService;
        this.provider = provider;
    }

    @Override
    public JwtTokenDto auth(UserCredentials credentials) {
        try {
            User user = userService.findOne(credentials.getUsername());

            if (user == null)
                throw new UsernameNotFoundException("User not found.");

            this.authenticate(credentials.getUsername(), credentials.getPassword());
            JwtTokenDto jwtToken = provider.generateToken(user);
            // ebnut existing
            // create new one
            return jwtToken;
        } catch (ApplicationException e) {
            throw e;
        } catch (UsernameNotFoundException | BadCredentialsException e) {
            throw new ExceptionDirector()
                    .applyBuilder(new AuthExceptionBuilder("Имя пользователя или пароль не верный"))
                    .build();
        } catch (Exception e) {
            log.error("IN findAll: {}", e.getMessage(), e);
            throw new ExceptionDirector()
                    .applyBuilder(new ApplicationExceptionBuilder(e))
                    .build();
        }
    }

    @Override
    public JwtTokenDto validateToken(String token) {
        try {
            return new JwtTokenDto(provider.validateToken(token));
        } catch (ApplicationException e) {
            throw e;
        } catch (Exception e) {
            log.error("IN findAll: {}", e.getMessage(), e);
            throw new ExceptionDirector()
                    .applyBuilder(new ApplicationExceptionBuilder(e))
                    .build();
        }
    }

    private void authenticate(String username, String password) {
        Objects.requireNonNull(username);
        Objects.requireNonNull(password);
        authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(username, password));
    }

}
