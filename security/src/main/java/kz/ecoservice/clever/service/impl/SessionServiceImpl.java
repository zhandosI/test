package kz.ecoservice.clever.service.impl;

import kz.ecoservice.clever.model.entity.Session;
import kz.ecoservice.clever.model.exception.ApplicationException;
import kz.ecoservice.clever.model.exception.builder.ApplicationExceptionBuilder;
import kz.ecoservice.clever.model.exception.builder.ExceptionDirector;
import kz.ecoservice.clever.repository.SessionRepository;
import kz.ecoservice.clever.service.SessionService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@Slf4j
public class SessionServiceImpl implements SessionService {

    private SessionRepository sessionRepository;

    @Autowired
    public void setSessionRepository(SessionRepository sessionRepository) {
        this.sessionRepository = sessionRepository;
    }

    @Override
    public List<Session> findAll() {
        try {
            return sessionRepository.findAll();
        } catch (ApplicationException e) {
            throw e;
        } catch (Exception e) {
            log.error("IN findAll: {}", e.getMessage(), e);
            throw new ExceptionDirector()
                    .applyBuilder(new ApplicationExceptionBuilder(e))
                    .build();
        }
    }

}
