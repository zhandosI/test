package kz.ecoservice.clever.controller;

import kz.ecoservice.clever.model.UserCredentials;
import kz.ecoservice.clever.model.dto.JwtTokenDto;
import kz.ecoservice.clever.service.SecurityService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;


@RestController
@RequestMapping("/api/v1/auth")
@Slf4j
public class AuthRestController extends BaseController {

    private SecurityService securityService;

    @Autowired
    public void setSecurityService(SecurityService securityService) {
        this.securityService = securityService;
    }

    @PostMapping(value = "", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<?> auth(@RequestBody final UserCredentials credentials) {
        return success()
                .withBody(securityService.auth(credentials))
                .build();
    }

    @PostMapping(value = "/token", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<?> validateToken(@RequestBody final JwtTokenDto jwtTokenDto) {
        return success()
                .withBody(securityService.validateToken(jwtTokenDto.getToken()))
                .build();
    }

}
