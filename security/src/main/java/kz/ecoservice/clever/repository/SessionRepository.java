package kz.ecoservice.clever.repository;

import kz.ecoservice.clever.model.entity.Session;
import org.springframework.data.jpa.repository.JpaRepository;

public interface SessionRepository extends JpaRepository<Session, Long> {
}
