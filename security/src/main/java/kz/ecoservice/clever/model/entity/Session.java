package kz.ecoservice.clever.model.entity;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.util.Date;

@Entity
@Table(name = "user_sessions")
@Getter
@Setter
@ToString
public class Session extends BaseEntity {

    @Column(name = "start_date", nullable = false)
    private Date startDate;

    @Column(name = "end_date")
    private Date endDate;

    @Column(name = "active", nullable = false)
    private Boolean isActive;

}
