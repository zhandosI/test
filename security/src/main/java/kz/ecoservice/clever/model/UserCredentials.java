package kz.ecoservice.clever.model;

import lombok.Data;

@Data
public class UserCredentials {

    private String username;
    private String password;

}
