package kz.ecoservice.clever.model;

import kz.ecoservice.clever.model.enm.Status;
import kz.ecoservice.clever.model.entity.User;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

import java.util.Collections;


public final class JwtUserFactory {

    public JwtUserFactory() {
    }

    public static JwtUser create(User user) {
        return new JwtUser(user.getUsername(),
                user.getPassword(),
                user.getStatus().equals(Status.ACTIVE),
                Collections.singletonList(new SimpleGrantedAuthority(user.getRole().getCode())));
    }

}
