package kz.ecoservice.clever.model.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.Date;

@Data
@AllArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class JwtTokenDto {

    private String token;
    private Date createdAt;
    private Long expiresIn;
    private Boolean isValid;

    public JwtTokenDto(Boolean isValid) {
        this.isValid = isValid;
    }

}
