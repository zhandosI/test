package kz.ecoservice.clever.util;

import org.junit.Test;

import static org.junit.Assert.*;


public class ExceptionUtilTest {

    @Test
    public void getRootCauseTest_success() {
        Throwable rootCause = new Throwable();
        Throwable cause = new Throwable(rootCause);
        Throwable ex = new Throwable(cause);
        Throwable expectedRootCause = ExceptionUtil.getRootCause(ex);
        assertEquals(rootCause, expectedRootCause);
    }

    @Test
    public void getRootCauseTest_null() {
        Throwable rootCause = ExceptionUtil.getRootCause(null);
        assertNull(rootCause);
    }

    @Test
    public void getRootCauseTest_cause_null() {
        Throwable th = new Throwable();
        Throwable rootCause = ExceptionUtil.getRootCause(th);
        assertEquals(rootCause, th);
    }

    @Test
    public void getPackageRootStackElementTest_null() {
        StackTraceElement ste = ExceptionUtil.getPackageRootStackElement(null, null);
        assertNull(ste);
    }

    @Test
    public void getPackageRootStackElementTest_package_null() {
        Throwable th = new Throwable();
        StackTraceElement ste = ExceptionUtil.getPackageRootStackElement(th, null);
        assertNull(ste);
    }

    @Test
    public void getPackageRootStackElementTest_success() {
        Throwable th = new Throwable();
        StackTraceElement ste = ExceptionUtil.getPackageRootStackElement(th, "kz");
        assertNotNull(ste);
    }

    @Test
    public void getPackageRootStackElementTest_package_not_exist() {
        Throwable th = new Throwable();
        StackTraceElement ste = ExceptionUtil.getPackageRootStackElement(th, "not_existing_package");
        assertNull(ste);
    }

}
