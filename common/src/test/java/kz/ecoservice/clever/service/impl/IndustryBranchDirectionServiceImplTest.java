package kz.ecoservice.clever.service.impl;

import kz.ecoservice.clever.model.ErrorCode;
import kz.ecoservice.clever.model.entity.IndustryBranchDirection;
import kz.ecoservice.clever.model.exception.ApplicationException;
import kz.ecoservice.clever.repository.IndustryBranchDirectionRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static org.junit.Assert.*;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class IndustryBranchDirectionServiceImplTest {


    private static final Long TEST_DIRECTION_ID = 69L;
    private static final String TEST_DIRECTION_NAME = "TEST_DIRECTION_NAME";

    @InjectMocks
    private IndustryBranchDirectionServiceImpl directionService;

    @Mock
    private IndustryBranchDirectionRepository directionRepository;

    @Before
    public void setUp() {
        IndustryBranchDirection entity = new IndustryBranchDirection();
        entity.setId(TEST_DIRECTION_ID);
        entity.setName(TEST_DIRECTION_NAME);

        when(directionRepository.getOne(TEST_DIRECTION_ID)).thenReturn(entity);
        when(directionRepository.findById(TEST_DIRECTION_ID)).thenReturn(Optional.of(entity));
    }

    @Test
    public void findOneTest_success() {
        IndustryBranchDirection operation = directionService.findOne(TEST_DIRECTION_ID);
        assertNotNull(operation);
        assertNotNull(operation.getId());
    }

    @Test(expected = ApplicationException.class)
    public void findOneTest_notFound() {
        Long notFoundId = 77L;
        try {
            directionService.findOne(notFoundId);
        } catch (ApplicationException ae) {
            assertEquals(ErrorCode.RESOURCE_NOT_FOUND, ae.getErrorCode());
            throw ae;
        }
    }

    @Test(expected = ApplicationException.class)
    public void findOneTest_unexpected_exception() {
        try {
            directionService.findOne(null);
        } catch (ApplicationException ae) {
            assertEquals(ErrorCode.INTERNAL_ERROR, ae.getErrorCode());
            throw ae;
        }
    }

    @Test
    public void findAllTest_success() {
        when(directionRepository.findAll()).thenReturn(Collections.singletonList(new IndustryBranchDirection()));
        List<IndustryBranchDirection> direction1 = directionService.findAll();
        assertFalse(direction1.isEmpty());

        when(directionRepository.findAll()).thenReturn(Collections.emptyList());
        List<IndustryBranchDirection> direction2 = directionService.findAll();
        assertTrue(direction2.isEmpty());

        assertNotNull(direction1);
        assertNotNull(direction2);
    }

    @Test(expected = ApplicationException.class)
    public void findAllTest_application_exception() {
        ApplicationException ae = new ApplicationException();
        ae.setErrorCode(ErrorCode.RESOURCE_NOT_FOUND);
        when(directionRepository.findAll()).thenThrow(ae);

        try {
            directionService.findAll();
        } catch (ApplicationException e) {
            assertEquals(ErrorCode.RESOURCE_NOT_FOUND, e.getErrorCode());
            throw e;
        }
    }

    @Test(expected = ApplicationException.class)
    public void findAllTest_unexpected_exception() {
        when(directionRepository.findAll()).thenThrow(new IllegalArgumentException("Illegal"));
        try {
            directionService.findAll();
        } catch (ApplicationException e) {
            assertEquals(ErrorCode.INTERNAL_ERROR, e.getErrorCode());
            assertEquals(e.getMessage(), "Illegal");
            throw e;
        }
    }

    @Test
    public void saveTest_success() {
        IndustryBranchDirection operation = new IndustryBranchDirection();
        operation.setName("Foobar");
        directionService.save(operation);
    }

    @Test(expected = ApplicationException.class)
    public void saveTest_unexpected_exception() {
        IndustryBranchDirection operation = new IndustryBranchDirection();
        try {
            directionService.save(operation);
        } catch (ApplicationException e) {
            assertEquals("java.lang.IllegalArgumentException", e.getRootCause().getExceptionName());
            assertEquals(ErrorCode.INTERNAL_ERROR, e.getErrorCode());
            throw e;
        }
    }

    @Test(expected = ApplicationException.class)
    public void saveTest_application_exception() {
        IndustryBranchDirection operation = new IndustryBranchDirection();
        operation.setName("test");

        ApplicationException ae = new ApplicationException();
        ae.setErrorCode(ErrorCode.RESOURCE_NOT_FOUND);

        when(directionRepository.save(operation)).thenThrow(ae);

        try {
            directionService.save(operation);
        } catch (ApplicationException e) {
            assertEquals(ErrorCode.RESOURCE_NOT_FOUND, e.getErrorCode());
            throw e;
        }
    }

    @Test
    public void updateTest_success() {
        IndustryBranchDirection direction = new IndustryBranchDirection();
        direction.setName("Foobar");
        directionService.update(TEST_DIRECTION_ID, direction);
    }

    @Test(expected = ApplicationException.class)
    public void updateTest_null_id_and_object() {
        try {
            directionService.update(null, null);
        } catch (ApplicationException e) {
            assertEquals("java.lang.IllegalArgumentException", e.getRootCause().getExceptionName());
            assertEquals(ErrorCode.INTERNAL_ERROR, e.getErrorCode());
            throw e;
        }
    }

    @Test(expected = ApplicationException.class)
    public void updateTest_null_object() {
        try {
            directionService.update(TEST_DIRECTION_ID, null);
        } catch (ApplicationException e) {
            assertEquals("java.lang.IllegalArgumentException", e.getRootCause().getExceptionName());
            assertEquals(ErrorCode.INTERNAL_ERROR, e.getErrorCode());
            throw e;
        }
    }

    @Test(expected = ApplicationException.class)
    public void updateTest_null_id_exception() {
        IndustryBranchDirection direction = new IndustryBranchDirection();
        direction.setName("Foobar");

        try {
            directionService.update(null, direction);
        } catch (ApplicationException e) {
            assertEquals("java.lang.IllegalArgumentException", e.getRootCause().getExceptionName());
            assertEquals(ErrorCode.INTERNAL_ERROR, e.getErrorCode());
            throw e;
        }
    }

    @Test(expected = ApplicationException.class)
    public void updateTest_application_exception() {
        IndustryBranchDirection operation = new IndustryBranchDirection();
        operation.setName(TEST_DIRECTION_NAME);

        ApplicationException ae = new ApplicationException();
        ae.setErrorCode(ErrorCode.RESOURCE_NOT_FOUND);

        when(directionRepository.getOne(TEST_DIRECTION_ID)).thenThrow(ae);

        try {
            directionService.update(TEST_DIRECTION_ID, operation);
        } catch (ApplicationException e) {
            assertEquals(ErrorCode.RESOURCE_NOT_FOUND, e.getErrorCode());
            throw e;
        }
    }

    @Test(expected = ApplicationException.class)
    public void updateTest_not_found() {
        IndustryBranchDirection operation = new IndustryBranchDirection();
        operation.setName(TEST_DIRECTION_NAME);

        Long notFoundId = 77L;
        try {
            directionService.update(notFoundId, operation);
        } catch (ApplicationException e) {
            assertEquals(ErrorCode.RESOURCE_NOT_FOUND, e.getErrorCode());
            throw e;
        }
    }

    @Test
    public void deleteTest_success() {
        directionService.delete(TEST_DIRECTION_ID);
    }

    @Test(expected = ApplicationException.class)
    public void deleteTest_not_found() {
        IndustryBranchDirection operation = new IndustryBranchDirection();
        operation.setName(TEST_DIRECTION_NAME);

        Long notFoundId = 77L;
        try {
            directionService.delete(notFoundId);
        } catch (ApplicationException e) {
            assertEquals(ErrorCode.RESOURCE_NOT_FOUND, e.getErrorCode());
            throw e;
        }
    }

    @Test(expected = ApplicationException.class)
    public void deleteTest_null_id_exception() {
        try {
            directionService.delete(null);
        } catch (ApplicationException e) {
            assertEquals("java.lang.IllegalArgumentException", e.getRootCause().getExceptionName());
            assertEquals(ErrorCode.INTERNAL_ERROR, e.getErrorCode());
            throw e;
        }
    }

}
