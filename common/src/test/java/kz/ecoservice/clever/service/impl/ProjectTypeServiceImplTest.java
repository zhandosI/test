package kz.ecoservice.clever.service.impl;

import kz.ecoservice.clever.model.ErrorCode;
import kz.ecoservice.clever.model.entity.ProjectType;
import kz.ecoservice.clever.model.exception.ApplicationException;
import kz.ecoservice.clever.repository.ProjectTypeRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static org.junit.Assert.*;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class ProjectTypeServiceImplTest {

    private static final Long TEST_ID = 69L;
    private static final String TEST_NAME = "TEST_NAME";

    @InjectMocks
    private ProjectTypeServiceImpl projectTypeService;

    @Mock
    private ProjectTypeRepository projectTypeRepository;

    @Before
    public void setUp() {
        ProjectType projectType = new ProjectType();
        projectType.setId(TEST_ID);
        projectType.setName(TEST_NAME);

        when(projectTypeRepository.getOne(TEST_ID)).thenReturn(projectType);
        when(projectTypeRepository.findById(TEST_ID)).thenReturn(Optional.of(projectType));
    }

    @Test
    public void findOneTest_success() {
        ProjectType projectType = projectTypeService.findOne(TEST_ID);
        assertNotNull(projectType);
        assertNotNull(projectType.getId());
    }

    @Test(expected = ApplicationException.class)
    public void findOneTest_notFound() {
        Long notFoundId = 77L;
        try {
            projectTypeService.findOne(notFoundId);
        } catch (ApplicationException ae) {
            assertEquals(ErrorCode.RESOURCE_NOT_FOUND, ae.getErrorCode());
            throw ae;
        }
    }

    @Test(expected = ApplicationException.class)
    public void findOneTest_unexpected_exception() {
        try {
            projectTypeService.findOne(null);
        } catch (ApplicationException ae) {
            assertEquals(ErrorCode.INTERNAL_ERROR, ae.getErrorCode());
            throw ae;
        }
    }

    @Test
    public void findAllTest_success() {
        when(projectTypeRepository.findAll()).thenReturn(Collections.singletonList(new ProjectType()));
        List<ProjectType> projectTypes1 = projectTypeService.findAll();
        assertFalse(projectTypes1.isEmpty());

        when(projectTypeRepository.findAll()).thenReturn(Collections.emptyList());
        List<ProjectType> projectTypes2 = projectTypeService.findAll();
        assertTrue(projectTypes2.isEmpty());

        assertNotNull(projectTypes1);
        assertNotNull(projectTypes2);
    }

    @Test(expected = ApplicationException.class)
    public void findAllTest_application_exception() {
        ApplicationException ae = new ApplicationException();
        ae.setErrorCode(ErrorCode.RESOURCE_NOT_FOUND);
        when(projectTypeRepository.findAll()).thenThrow(ae);

        try {
            projectTypeService.findAll();
        } catch (ApplicationException e) {
            assertEquals(ErrorCode.RESOURCE_NOT_FOUND, e.getErrorCode());
            throw e;
        }
    }

    @Test(expected = ApplicationException.class)
    public void findAllTest_unexpected_exception() {
        when(projectTypeRepository.findAll()).thenThrow(new IllegalArgumentException("Illegal"));
        try {
            projectTypeService.findAll();
        } catch (ApplicationException e) {
            assertEquals(ErrorCode.INTERNAL_ERROR, e.getErrorCode());
            assertEquals(e.getMessage(), "Illegal");
            throw e;
        }
    }

    @Test
    public void saveTest_success() {
        ProjectType projectType = new ProjectType();
        projectType.setName(TEST_NAME);
        projectTypeService.save(projectType);
    }

    @Test(expected = ApplicationException.class)
    public void saveTest_unexpected_exception() {
        ProjectType projectType = new ProjectType();
        try {
            projectTypeService.save(projectType);
        } catch (ApplicationException e) {
            assertEquals("java.lang.IllegalArgumentException", e.getRootCause().getExceptionName());
            assertEquals(ErrorCode.INTERNAL_ERROR, e.getErrorCode());
            throw e;
        }
    }

    @Test(expected = ApplicationException.class)
    public void saveTest_application_exception() {
        ProjectType projectType = new ProjectType();
        projectType.setName(TEST_NAME);

        ApplicationException ae = new ApplicationException();
        ae.setErrorCode(ErrorCode.RESOURCE_NOT_FOUND);

        when(projectTypeRepository.save(projectType)).thenThrow(ae);

        try {
            projectTypeService.save(projectType);
        } catch (ApplicationException e) {
            assertEquals(ErrorCode.RESOURCE_NOT_FOUND, e.getErrorCode());
            throw e;
        }
    }

    @Test
    public void updateTest_success() {
        ProjectType projectType = new ProjectType();
        projectType.setName(TEST_NAME);
        projectTypeService.update(TEST_ID, projectType);
    }

    @Test(expected = ApplicationException.class)
    public void updateTest_null_id_and_object() {
        try {
            projectTypeService.update(null, null);
        } catch (ApplicationException e) {
            assertEquals("java.lang.IllegalArgumentException", e.getRootCause().getExceptionName());
            assertEquals(ErrorCode.INTERNAL_ERROR, e.getErrorCode());
            throw e;
        }
    }

    @Test(expected = ApplicationException.class)
    public void updateTest_null_object() {
        try {
            projectTypeService.update(TEST_ID, null);
        } catch (ApplicationException e) {
            assertEquals("java.lang.IllegalArgumentException", e.getRootCause().getExceptionName());
            assertEquals(ErrorCode.INTERNAL_ERROR, e.getErrorCode());
            throw e;
        }
    }

    @Test(expected = ApplicationException.class)
    public void updateTest_null_id_exception() {
        ProjectType projectType = new ProjectType();
        projectType.setName(TEST_NAME);

        try {
            projectTypeService.update(null, projectType);
        } catch (ApplicationException e) {
            assertEquals("java.lang.IllegalArgumentException", e.getRootCause().getExceptionName());
            assertEquals(ErrorCode.INTERNAL_ERROR, e.getErrorCode());
            throw e;
        }
    }

    @Test(expected = ApplicationException.class)
    public void updateTest_application_exception() {
        ProjectType projectType = new ProjectType();
        projectType.setName(TEST_NAME);

        ApplicationException ae = new ApplicationException();
        ae.setErrorCode(ErrorCode.RESOURCE_NOT_FOUND);

        when(projectTypeRepository.getOne(TEST_ID)).thenThrow(ae);

        try {
            projectTypeService.update(TEST_ID, projectType);
        } catch (ApplicationException e) {
            assertEquals(ErrorCode.RESOURCE_NOT_FOUND, e.getErrorCode());
            throw e;
        }
    }

    @Test(expected = ApplicationException.class)
    public void updateTest_not_found() {
        ProjectType projectType = new ProjectType();
        projectType.setName(TEST_NAME);

        Long notFoundId = 77L;
        try {
            projectTypeService.update(notFoundId, projectType);
        } catch (ApplicationException e) {
            assertEquals(ErrorCode.RESOURCE_NOT_FOUND, e.getErrorCode());
            throw e;
        }
    }

    @Test
    public void deleteTest_success() {
        projectTypeService.delete(TEST_ID);
    }

    @Test(expected = ApplicationException.class)
    public void deleteTest_not_found() {
        ProjectType projectType = new ProjectType();
        projectType.setName(TEST_NAME);

        Long notFoundId = 77L;
        try {
            projectTypeService.delete(notFoundId);
        } catch (ApplicationException e) {
            assertEquals(ErrorCode.RESOURCE_NOT_FOUND, e.getErrorCode());
            throw e;
        }
    }

    @Test(expected = ApplicationException.class)
    public void deleteTest_null_id_exception() {
        try {
            projectTypeService.delete(null);
        } catch (ApplicationException e) {
            assertEquals("java.lang.IllegalArgumentException", e.getRootCause().getExceptionName());
            assertEquals(ErrorCode.INTERNAL_ERROR, e.getErrorCode());
            throw e;
        }
    }

}
