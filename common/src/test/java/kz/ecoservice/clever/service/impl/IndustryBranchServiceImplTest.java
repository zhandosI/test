package kz.ecoservice.clever.service.impl;

import kz.ecoservice.clever.model.ErrorCode;
import kz.ecoservice.clever.model.entity.IndustryBranch;
import kz.ecoservice.clever.model.exception.ApplicationException;
import kz.ecoservice.clever.repository.IndustryBranchRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static org.junit.Assert.*;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class IndustryBranchServiceImplTest {

    private static final Long TEST_ID = 69L;
    private static final String TEST_NAME = "TEST_NAME";

    @InjectMocks
    private IndustryBranchServiceImpl industryBranchService;

    @Mock
    private IndustryBranchRepository industryBranchRepository;

    @Before
    public void setUp() {
        IndustryBranch industryBranch= new IndustryBranch();
        industryBranch.setId(TEST_ID);
        industryBranch.setName(TEST_NAME);

        when(industryBranchRepository.getOne(TEST_ID)).thenReturn(industryBranch);
        when(industryBranchRepository.findById(TEST_ID)).thenReturn(Optional.of(industryBranch));
    }

    @Test
    public void findOneTest_success() {
        IndustryBranch industryBranch = industryBranchService.findOne(TEST_ID);
        assertNotNull(industryBranch);
        assertNotNull(industryBranch.getId());
    }

    @Test(expected = ApplicationException.class)
    public void findOneTest_notFound() {
        Long notFoundId = 77L;
        try {
            industryBranchService.findOne(notFoundId);
        } catch (ApplicationException ae) {
            assertEquals(ErrorCode.RESOURCE_NOT_FOUND, ae.getErrorCode());
            throw ae;
        }
    }

    @Test(expected = ApplicationException.class)
    public void findOneTest_unexpected_exception() {
        try {
            industryBranchService.findOne(null);
        } catch (ApplicationException ae) {
            assertEquals(ErrorCode.INTERNAL_ERROR, ae.getErrorCode());
            throw ae;
        }
    }

    @Test
    public void findAllTest_success() {
        when(industryBranchRepository.findAll()).thenReturn(Collections.singletonList(new IndustryBranch()));
        List<IndustryBranch> industryBranches1 = industryBranchService.findAll();
        assertFalse(industryBranches1.isEmpty());

        when(industryBranchRepository.findAll()).thenReturn(Collections.emptyList());
        List<IndustryBranch> industryBranches2 = industryBranchService.findAll();
        assertTrue(industryBranches2.isEmpty());

        assertNotNull(industryBranches1);
        assertNotNull(industryBranches2);
    }

    @Test(expected = ApplicationException.class)
    public void findAllTest_application_exception() {
        ApplicationException ae = new ApplicationException();
        ae.setErrorCode(ErrorCode.RESOURCE_NOT_FOUND);
        when(industryBranchRepository.findAll()).thenThrow(ae);

        try {
            industryBranchService.findAll();
        } catch (ApplicationException e) {
            assertEquals(ErrorCode.RESOURCE_NOT_FOUND, e.getErrorCode());
            throw e;
        }
    }

    @Test(expected = ApplicationException.class)
    public void findAllTest_unexpected_exception() {
        when(industryBranchRepository.findAll()).thenThrow(new IllegalArgumentException("Illegal"));
        try {
            industryBranchService.findAll();
        } catch (ApplicationException e) {
            assertEquals(ErrorCode.INTERNAL_ERROR, e.getErrorCode());
            assertEquals(e.getMessage(), "Illegal");
            throw e;
        }
    }

    @Test
    public void saveTest_success() {
        IndustryBranch industryBranch = new IndustryBranch();
        industryBranch.setName(TEST_NAME);
        industryBranchService.save(industryBranch);
    }

    @Test(expected = ApplicationException.class)
    public void saveTest_unexpected_exception() {
        IndustryBranch industryBranch = new IndustryBranch();
        try {
            industryBranchService.save(industryBranch);
        } catch (ApplicationException e) {
            assertEquals("java.lang.IllegalArgumentException", e.getRootCause().getExceptionName());
            assertEquals(ErrorCode.INTERNAL_ERROR, e.getErrorCode());
            throw e;
        }
    }

    @Test(expected = ApplicationException.class)
    public void saveTest_application_exception() {
        IndustryBranch industryBranch = new IndustryBranch();
        industryBranch.setName(TEST_NAME);

        ApplicationException ae = new ApplicationException();
        ae.setErrorCode(ErrorCode.RESOURCE_NOT_FOUND);

        when(industryBranchRepository.save(industryBranch)).thenThrow(ae);

        try {
            industryBranchService.save(industryBranch);
        } catch (ApplicationException e) {
            assertEquals(ErrorCode.RESOURCE_NOT_FOUND, e.getErrorCode());
            throw e;
        }
    }

    @Test
    public void updateTest_success() {
        IndustryBranch industryBranch = new IndustryBranch();
        industryBranch.setName(TEST_NAME);
        industryBranchService.update(TEST_ID, industryBranch);
    }

    @Test(expected = ApplicationException.class)
    public void updateTest_null_id_and_object() {
        try {
            industryBranchService.update(null, null);
        } catch (ApplicationException e) {
            assertEquals("java.lang.IllegalArgumentException", e.getRootCause().getExceptionName());
            assertEquals(ErrorCode.INTERNAL_ERROR, e.getErrorCode());
            throw e;
        }
    }

    @Test(expected = ApplicationException.class)
    public void updateTest_null_object() {
        try {
            industryBranchService.update(TEST_ID, null);
        } catch (ApplicationException e) {
            assertEquals("java.lang.IllegalArgumentException", e.getRootCause().getExceptionName());
            assertEquals(ErrorCode.INTERNAL_ERROR, e.getErrorCode());
            throw e;
        }
    }

    @Test(expected = ApplicationException.class)
    public void updateTest_null_id_exception() {
        IndustryBranch industryBranch = new IndustryBranch();
        industryBranch.setName(TEST_NAME);

        try {
            industryBranchService.update(null, industryBranch);
        } catch (ApplicationException e) {
            assertEquals("java.lang.IllegalArgumentException", e.getRootCause().getExceptionName());
            assertEquals(ErrorCode.INTERNAL_ERROR, e.getErrorCode());
            throw e;
        }
    }

    @Test(expected = ApplicationException.class)
    public void updateTest_application_exception() {
        IndustryBranch industryBranch = new IndustryBranch();
        industryBranch.setName(TEST_NAME);

        ApplicationException ae = new ApplicationException();
        ae.setErrorCode(ErrorCode.RESOURCE_NOT_FOUND);

        when(industryBranchRepository.getOne(TEST_ID)).thenThrow(ae);

        try {
            industryBranchService.update(TEST_ID, industryBranch);
        } catch (ApplicationException e) {
            assertEquals(ErrorCode.RESOURCE_NOT_FOUND, e.getErrorCode());
            throw e;
        }
    }

    @Test(expected = ApplicationException.class)
    public void updateTest_not_found() {
        IndustryBranch industryBranch = new IndustryBranch();
        industryBranch.setName(TEST_NAME);

        Long notFoundId = 77L;
        try {
            industryBranchService.update(notFoundId, industryBranch);
        } catch (ApplicationException e) {
            assertEquals(ErrorCode.RESOURCE_NOT_FOUND, e.getErrorCode());
            throw e;
        }
    }

    @Test
    public void deleteTest_success() {
        industryBranchService.delete(TEST_ID);
    }

    @Test(expected = ApplicationException.class)
    public void deleteTest_not_found() {
        IndustryBranch industryBranch = new IndustryBranch();
        industryBranch.setName(TEST_NAME);

        Long notFoundId = 77L;
        try {
            industryBranchService.delete(notFoundId);
        } catch (ApplicationException e) {
            assertEquals(ErrorCode.RESOURCE_NOT_FOUND, e.getErrorCode());
            throw e;
        }
    }

    @Test(expected = ApplicationException.class)
    public void deleteTest_null_id_exception() {
        try {
            industryBranchService.delete(null);
        } catch (ApplicationException e) {
            assertEquals("java.lang.IllegalArgumentException", e.getRootCause().getExceptionName());
            assertEquals(ErrorCode.INTERNAL_ERROR, e.getErrorCode());
            throw e;
        }
    }

}
