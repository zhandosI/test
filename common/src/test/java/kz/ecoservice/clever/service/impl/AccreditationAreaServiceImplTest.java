package kz.ecoservice.clever.service.impl;

import kz.ecoservice.clever.model.ErrorCode;
import kz.ecoservice.clever.model.entity.AccreditationArea;
import kz.ecoservice.clever.model.exception.ApplicationException;
import kz.ecoservice.clever.repository.AccreditationAreaRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static org.junit.Assert.*;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class AccreditationAreaServiceImplTest {

    private static final Long TEST_ID = 69L;
    private static final String TEST_NAME = "TEST_NAME";

    @InjectMocks
    private AccreditationAreaServiceImpl accreditationAreaService;

    @Mock
    private AccreditationAreaRepository accreditationAreaRepository;

    @Before
    public void setUp() {
        AccreditationArea accreditationArea = new AccreditationArea();
        accreditationArea.setId(TEST_ID);
        accreditationArea.setName(TEST_NAME);

        when(accreditationAreaRepository.getOne(TEST_ID)).thenReturn(accreditationArea);
        when(accreditationAreaRepository.findById(TEST_ID)).thenReturn(Optional.of(accreditationArea));
    }

    @Test
    public void findOneTest_success() {
        AccreditationArea accreditationArea = accreditationAreaService.findOne(TEST_ID);
        assertNotNull(accreditationArea);
        assertNotNull(accreditationArea.getId());
    }

    @Test(expected = ApplicationException.class)
    public void findOneTest_notFound() {
        Long notFoundId = 77L;
        try {
            accreditationAreaService.findOne(notFoundId);
        } catch (ApplicationException ae) {
            assertEquals(ErrorCode.RESOURCE_NOT_FOUND, ae.getErrorCode());
            throw ae;
        }
    }

    @Test(expected = ApplicationException.class)
    public void findOneTest_unexpected_exception() {
        try {
            accreditationAreaService.findOne(null);
        } catch (ApplicationException ae) {
            assertEquals(ErrorCode.INTERNAL_ERROR, ae.getErrorCode());
            throw ae;
        }
    }

    @Test
    public void findAllTest_success() {
        when(accreditationAreaRepository.findAll()).thenReturn(Collections.singletonList(new AccreditationArea()));
        List<AccreditationArea> accreditationAreas1 = accreditationAreaService.findAll();
        assertFalse(accreditationAreas1.isEmpty());

        when(accreditationAreaRepository.findAll()).thenReturn(Collections.emptyList());
        List<AccreditationArea> accreditationAreas2 = accreditationAreaService.findAll();
        assertTrue(accreditationAreas2.isEmpty());

        assertNotNull(accreditationAreas1);
        assertNotNull(accreditationAreas2);
    }

    @Test(expected = ApplicationException.class)
    public void findAllTest_application_exception() {
        ApplicationException ae = new ApplicationException();
        ae.setErrorCode(ErrorCode.RESOURCE_NOT_FOUND);
        when(accreditationAreaRepository.findAll()).thenThrow(ae);

        try {
            accreditationAreaService.findAll();
        } catch (ApplicationException e) {
            assertEquals(ErrorCode.RESOURCE_NOT_FOUND, e.getErrorCode());
            throw e;
        }
    }

    @Test(expected = ApplicationException.class)
    public void findAllTest_unexpected_exception() {
        when(accreditationAreaRepository.findAll()).thenThrow(new IllegalArgumentException("Illegal"));
        try {
            accreditationAreaService.findAll();
        } catch (ApplicationException e) {
            assertEquals(ErrorCode.INTERNAL_ERROR, e.getErrorCode());
            assertEquals(e.getMessage(), "Illegal");
            throw e;
        }
    }

    @Test
    public void saveTest_success() {
        AccreditationArea accreditationArea = new AccreditationArea();
        accreditationArea.setName(TEST_NAME);
        accreditationAreaService.save(accreditationArea);
    }

    @Test(expected = ApplicationException.class)
    public void saveTest_unexpected_exception() {
        AccreditationArea accreditationArea = new AccreditationArea();
        try {
            accreditationAreaService.save(accreditationArea);
        } catch (ApplicationException e) {
            assertEquals("java.lang.IllegalArgumentException", e.getRootCause().getExceptionName());
            assertEquals(ErrorCode.INTERNAL_ERROR, e.getErrorCode());
            throw e;
        }
    }

    @Test(expected = ApplicationException.class)
    public void saveTest_application_exception() {
        AccreditationArea accreditationArea = new AccreditationArea();
        accreditationArea.setName(TEST_NAME);

        ApplicationException ae = new ApplicationException();
        ae.setErrorCode(ErrorCode.RESOURCE_NOT_FOUND);

        when(accreditationAreaRepository.save(accreditationArea)).thenThrow(ae);

        try {
            accreditationAreaService.save(accreditationArea);
        } catch (ApplicationException e) {
            assertEquals(ErrorCode.RESOURCE_NOT_FOUND, e.getErrorCode());
            throw e;
        }
    }

    @Test
    public void updateTest_success() {
        AccreditationArea accreditationArea = new AccreditationArea();
        accreditationArea.setName(TEST_NAME);
        accreditationAreaService.update(TEST_ID, accreditationArea);
    }

    @Test(expected = ApplicationException.class)
    public void updateTest_null_id_and_object() {
        try {
            accreditationAreaService.update(null, null);
        } catch (ApplicationException e) {
            assertEquals("java.lang.IllegalArgumentException", e.getRootCause().getExceptionName());
            assertEquals(ErrorCode.INTERNAL_ERROR, e.getErrorCode());
            throw e;
        }
    }

    @Test(expected = ApplicationException.class)
    public void updateTest_null_object() {
        try {
            accreditationAreaService.update(TEST_ID, null);
        } catch (ApplicationException e) {
            assertEquals("java.lang.IllegalArgumentException", e.getRootCause().getExceptionName());
            assertEquals(ErrorCode.INTERNAL_ERROR, e.getErrorCode());
            throw e;
        }
    }

    @Test(expected = ApplicationException.class)
    public void updateTest_null_id_exception() {
        AccreditationArea accreditationArea = new AccreditationArea();
        accreditationArea.setName(TEST_NAME);

        try {
            accreditationAreaService.update(null, accreditationArea);
        } catch (ApplicationException e) {
            assertEquals("java.lang.IllegalArgumentException", e.getRootCause().getExceptionName());
            assertEquals(ErrorCode.INTERNAL_ERROR, e.getErrorCode());
            throw e;
        }
    }

    @Test(expected = ApplicationException.class)
    public void updateTest_application_exception() {
        AccreditationArea accreditationArea = new AccreditationArea();
        accreditationArea.setName(TEST_NAME);

        ApplicationException ae = new ApplicationException();
        ae.setErrorCode(ErrorCode.RESOURCE_NOT_FOUND);

        when(accreditationAreaRepository.getOne(TEST_ID)).thenThrow(ae);

        try {
            accreditationAreaService.update(TEST_ID, accreditationArea);
        } catch (ApplicationException e) {
            assertEquals(ErrorCode.RESOURCE_NOT_FOUND, e.getErrorCode());
            throw e;
        }
    }

    @Test(expected = ApplicationException.class)
    public void updateTest_not_found() {
        AccreditationArea accreditationArea = new AccreditationArea();
        accreditationArea.setName(TEST_NAME);

        Long notFoundId = 77L;
        try {
            accreditationAreaService.update(notFoundId, accreditationArea);
        } catch (ApplicationException e) {
            assertEquals(ErrorCode.RESOURCE_NOT_FOUND, e.getErrorCode());
            throw e;
        }
    }

    @Test
    public void deleteTest_success() {
        accreditationAreaService.delete(TEST_ID);
    }

    @Test(expected = ApplicationException.class)
    public void deleteTest_not_found() {
        AccreditationArea accreditationArea = new AccreditationArea();
        accreditationArea.setName(TEST_NAME);

        Long notFoundId = 77L;
        try {
            accreditationAreaService.delete(notFoundId);
        } catch (ApplicationException e) {
            assertEquals(ErrorCode.RESOURCE_NOT_FOUND, e.getErrorCode());
            throw e;
        }
    }

    @Test(expected = ApplicationException.class)
    public void deleteTest_null_id_exception() {
        try {
            accreditationAreaService.delete(null);
        } catch (ApplicationException e) {
            assertEquals("java.lang.IllegalArgumentException", e.getRootCause().getExceptionName());
            assertEquals(ErrorCode.INTERNAL_ERROR, e.getErrorCode());
            throw e;
        }
    }

}
