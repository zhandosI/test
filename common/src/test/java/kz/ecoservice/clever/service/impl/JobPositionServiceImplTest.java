package kz.ecoservice.clever.service.impl;

import kz.ecoservice.clever.model.ErrorCode;
import kz.ecoservice.clever.model.entity.JobPosition;
import kz.ecoservice.clever.model.exception.ApplicationException;
import kz.ecoservice.clever.repository.JobPositionRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static org.junit.Assert.*;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class JobPositionServiceImplTest {

    private static final Long TEST_ID = 69L;
    private static final String TEST_NAME = "TEST_NAME";

    @InjectMocks
    private JobPositionServiceImpl jobPositionService;

    @Mock
    private JobPositionRepository jobPositionRepository;

    @Before
    public void setUp() {
        JobPosition jobPosition = new JobPosition();
        jobPosition.setId(TEST_ID);
        jobPosition.setName(TEST_NAME);

        when(jobPositionRepository.getOne(TEST_ID)).thenReturn(jobPosition);
        when(jobPositionRepository.findById(TEST_ID)).thenReturn(Optional.of(jobPosition));
    }

    @Test
    public void findOneTest_success() {
        JobPosition jobPosition = jobPositionService.findOne(TEST_ID);
        assertNotNull(jobPosition);
        assertNotNull(jobPosition.getId());
    }

    @Test(expected = ApplicationException.class)
    public void findOneTest_notFound() {
        Long notFoundId = 77L;
        try {
            jobPositionService.findOne(notFoundId);
        } catch (ApplicationException ae) {
            assertEquals(ErrorCode.RESOURCE_NOT_FOUND, ae.getErrorCode());
            throw ae;
        }
    }

    @Test(expected = ApplicationException.class)
    public void findOneTest_unexpected_exception() {
        try {
            jobPositionService.findOne(null);
        } catch (ApplicationException ae) {
            assertEquals(ErrorCode.INTERNAL_ERROR, ae.getErrorCode());
            throw ae;
        }
    }

    @Test
    public void findAllTest_success() {
        when(jobPositionRepository.findAll()).thenReturn(Collections.singletonList(new JobPosition()));
        List<JobPosition> jobPositions1 = jobPositionService.findAll();
        assertFalse(jobPositions1.isEmpty());

        when(jobPositionRepository.findAll()).thenReturn(Collections.emptyList());
        List<JobPosition> jobPositions2 = jobPositionService.findAll();
        assertTrue(jobPositions2.isEmpty());

        assertNotNull(jobPositions1);
        assertNotNull(jobPositions2);
    }

    @Test(expected = ApplicationException.class)
    public void findAllTest_application_exception() {
        ApplicationException ae = new ApplicationException();
        ae.setErrorCode(ErrorCode.RESOURCE_NOT_FOUND);
        when(jobPositionRepository.findAll()).thenThrow(ae);

        try {
            jobPositionService.findAll();
        } catch (ApplicationException e) {
            assertEquals(ErrorCode.RESOURCE_NOT_FOUND, e.getErrorCode());
            throw e;
        }
    }

    @Test(expected = ApplicationException.class)
    public void findAllTest_unexpected_exception() {
        when(jobPositionRepository.findAll()).thenThrow(new IllegalArgumentException("Illegal"));
        try {
            jobPositionService.findAll();
        } catch (ApplicationException e) {
            assertEquals(ErrorCode.INTERNAL_ERROR, e.getErrorCode());
            assertEquals(e.getMessage(), "Illegal");
            throw e;
        }
    }

    @Test
    public void saveTest_success() {
        JobPosition jobPosition = new JobPosition();
        jobPosition.setName(TEST_NAME);
        jobPositionService.save(jobPosition);
    }

    @Test(expected = ApplicationException.class)
    public void saveTest_unexpected_exception() {
        JobPosition jobPosition = new JobPosition();
        try {
            jobPositionService.save(jobPosition);
        } catch (ApplicationException e) {
            assertEquals("java.lang.IllegalArgumentException", e.getRootCause().getExceptionName());
            assertEquals(ErrorCode.INTERNAL_ERROR, e.getErrorCode());
            throw e;
        }
    }

    @Test(expected = ApplicationException.class)
    public void saveTest_application_exception() {
        JobPosition jobPosition = new JobPosition();
        jobPosition.setName(TEST_NAME);

        ApplicationException ae = new ApplicationException();
        ae.setErrorCode(ErrorCode.RESOURCE_NOT_FOUND);

        when(jobPositionRepository.save(jobPosition)).thenThrow(ae);

        try {
            jobPositionService.save(jobPosition);
        } catch (ApplicationException e) {
            assertEquals(ErrorCode.RESOURCE_NOT_FOUND, e.getErrorCode());
            throw e;
        }
    }

    @Test
    public void updateTest_success() {
        JobPosition jobPosition = new JobPosition();
        jobPosition.setName(TEST_NAME);
        jobPositionService.update(TEST_ID, jobPosition);
    }

    @Test(expected = ApplicationException.class)
    public void updateTest_null_id_and_object() {
        try {
            jobPositionService.update(null, null);
        } catch (ApplicationException e) {
            assertEquals("java.lang.IllegalArgumentException", e.getRootCause().getExceptionName());
            assertEquals(ErrorCode.INTERNAL_ERROR, e.getErrorCode());
            throw e;
        }
    }

    @Test(expected = ApplicationException.class)
    public void updateTest_null_object() {
        try {
            jobPositionService.update(TEST_ID, null);
        } catch (ApplicationException e) {
            assertEquals("java.lang.IllegalArgumentException", e.getRootCause().getExceptionName());
            assertEquals(ErrorCode.INTERNAL_ERROR, e.getErrorCode());
            throw e;
        }
    }

    @Test(expected = ApplicationException.class)
    public void updateTest_null_id_exception() {
        JobPosition jobPosition = new JobPosition();
        jobPosition.setName(TEST_NAME);

        try {
            jobPositionService.update(null, jobPosition);
        } catch (ApplicationException e) {
            assertEquals("java.lang.IllegalArgumentException", e.getRootCause().getExceptionName());
            assertEquals(ErrorCode.INTERNAL_ERROR, e.getErrorCode());
            throw e;
        }
    }

    @Test(expected = ApplicationException.class)
    public void updateTest_application_exception() {
        JobPosition jobPosition = new JobPosition();
        jobPosition.setName(TEST_NAME);

        ApplicationException ae = new ApplicationException();
        ae.setErrorCode(ErrorCode.RESOURCE_NOT_FOUND);

        when(jobPositionRepository.getOne(TEST_ID)).thenThrow(ae);

        try {
            jobPositionService.update(TEST_ID, jobPosition);
        } catch (ApplicationException e) {
            assertEquals(ErrorCode.RESOURCE_NOT_FOUND, e.getErrorCode());
            throw e;
        }
    }

    @Test(expected = ApplicationException.class)
    public void updateTest_not_found() {
        JobPosition jobPosition = new JobPosition();
        jobPosition.setName(TEST_NAME);

        Long notFoundId = 77L;
        try {
            jobPositionService.update(notFoundId, jobPosition);
        } catch (ApplicationException e) {
            assertEquals(ErrorCode.RESOURCE_NOT_FOUND, e.getErrorCode());
            throw e;
        }
    }

    @Test
    public void deleteTest_success() {
        jobPositionService.delete(TEST_ID);
    }

    @Test(expected = ApplicationException.class)
    public void deleteTest_not_found() {
        JobPosition jobPosition = new JobPosition();
        jobPosition.setName(TEST_NAME);

        Long notFoundId = 77L;
        try {
            jobPositionService.delete(notFoundId);
        } catch (ApplicationException e) {
            assertEquals(ErrorCode.RESOURCE_NOT_FOUND, e.getErrorCode());
            throw e;
        }
    }

    @Test(expected = ApplicationException.class)
    public void deleteTest_null_id_exception() {
        try {
            jobPositionService.delete(null);
        } catch (ApplicationException e) {
            assertEquals("java.lang.IllegalArgumentException", e.getRootCause().getExceptionName());
            assertEquals(ErrorCode.INTERNAL_ERROR, e.getErrorCode());
            throw e;
        }
    }

}
