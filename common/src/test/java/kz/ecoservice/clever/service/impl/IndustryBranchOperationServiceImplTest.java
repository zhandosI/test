package kz.ecoservice.clever.service.impl;

import kz.ecoservice.clever.model.ErrorCode;
import kz.ecoservice.clever.model.entity.IndustryBranchOperation;
import kz.ecoservice.clever.model.exception.ApplicationException;
import kz.ecoservice.clever.repository.IndustryBranchOperationRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static org.junit.Assert.*;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class IndustryBranchOperationServiceImplTest {

    private static final Long TEST_OPERATION_ID = 69L;
    private static final String TEST_OPERATION_NAME = "TEST_OPERATION_NAME";

    @InjectMocks
    private IndustryBranchOperationServiceImpl operationService;

    @Mock
    private IndustryBranchOperationRepository operationRepository;

    @Before
    public void setUp() {
        IndustryBranchOperation entity = new IndustryBranchOperation();
        entity.setId(TEST_OPERATION_ID);
        entity.setName(TEST_OPERATION_NAME);

        when(operationRepository.getOne(TEST_OPERATION_ID)).thenReturn(entity);
        when(operationRepository.findById(TEST_OPERATION_ID)).thenReturn(Optional.of(entity));
    }

    @Test
    public void findOneTest_success() {
        IndustryBranchOperation operation = operationService.findOne(TEST_OPERATION_ID);
        assertNotNull(operation);
        assertNotNull(operation.getId());
    }

    @Test(expected = ApplicationException.class)
    public void findOneTest_notFound() {
        Long notFoundId = 77L;
        try {
            operationService.findOne(notFoundId);
        } catch (ApplicationException ae) {
            assertEquals(ErrorCode.RESOURCE_NOT_FOUND, ae.getErrorCode());
            throw ae;
        }
    }

    @Test(expected = ApplicationException.class)
    public void findOneTest_unexpected_exception() {
        try {
            operationService.findOne(null);
        } catch (ApplicationException ae) {
            assertEquals(ErrorCode.INTERNAL_ERROR, ae.getErrorCode());
            throw ae;
        }
    }

    @Test
    public void findAllTest_success() {
        when(operationRepository.findAll()).thenReturn(Collections.singletonList(new IndustryBranchOperation()));
        List<IndustryBranchOperation> operations1 = operationService.findAll();
        assertFalse(operations1.isEmpty());

        when(operationRepository.findAll()).thenReturn(Collections.emptyList());
        List<IndustryBranchOperation> operations2 = operationService.findAll();
        assertTrue(operations2.isEmpty());

        assertNotNull(operations1);
        assertNotNull(operations2);
    }

    @Test(expected = ApplicationException.class)
    public void findAllTest_application_exception() {
        ApplicationException ae = new ApplicationException();
        ae.setErrorCode(ErrorCode.RESOURCE_NOT_FOUND);
        when(operationRepository.findAll()).thenThrow(ae);

        try {
            operationService.findAll();
        } catch (ApplicationException e) {
            assertEquals(ErrorCode.RESOURCE_NOT_FOUND, e.getErrorCode());
            throw e;
        }
    }

    @Test(expected = ApplicationException.class)
    public void findAllTest_unexpected_exception() {
        when(operationRepository.findAll()).thenThrow(new IllegalArgumentException("Illegal"));
        try {
            operationService.findAll();
        } catch (ApplicationException e) {
            assertEquals(ErrorCode.INTERNAL_ERROR, e.getErrorCode());
            assertEquals(e.getMessage(), "Illegal");
            throw e;
        }
    }

    @Test
    public void saveTest_success() {
        IndustryBranchOperation operation = new IndustryBranchOperation();
        operation.setName("Foobar");
        operationService.save(operation);
    }

    @Test(expected = ApplicationException.class)
    public void saveTest_unexpected_exception() {
        IndustryBranchOperation operation = new IndustryBranchOperation();
        try {
            operationService.save(operation);
        } catch (ApplicationException e) {
            assertEquals("java.lang.IllegalArgumentException", e.getRootCause().getExceptionName());
            assertEquals(ErrorCode.INTERNAL_ERROR, e.getErrorCode());
            throw e;
        }
    }

    @Test(expected = ApplicationException.class)
    public void saveTest_application_exception() {
        IndustryBranchOperation operation = new IndustryBranchOperation();
        operation.setName("test");

        ApplicationException ae = new ApplicationException();
        ae.setErrorCode(ErrorCode.RESOURCE_NOT_FOUND);

        when(operationRepository.save(operation)).thenThrow(ae);

        try {
            operationService.save(operation);
        } catch (ApplicationException e) {
            assertEquals(ErrorCode.RESOURCE_NOT_FOUND, e.getErrorCode());
            throw e;
        }
    }

    @Test
    public void updateTest_success() {
        IndustryBranchOperation operation = new IndustryBranchOperation();
        operation.setName("Foobar");
        operationService.update(TEST_OPERATION_ID, operation);
    }

    @Test(expected = ApplicationException.class)
    public void updateTest_null_id_and_object() {
        try {
            operationService.update(null, null);
        } catch (ApplicationException e) {
            assertEquals("java.lang.IllegalArgumentException", e.getRootCause().getExceptionName());
            assertEquals(ErrorCode.INTERNAL_ERROR, e.getErrorCode());
            throw e;
        }
    }

    @Test(expected = ApplicationException.class)
    public void updateTest_null_object() {
        try {
            operationService.update(TEST_OPERATION_ID, null);
        } catch (ApplicationException e) {
            assertEquals("java.lang.IllegalArgumentException", e.getRootCause().getExceptionName());
            assertEquals(ErrorCode.INTERNAL_ERROR, e.getErrorCode());
            throw e;
        }
    }

    @Test(expected = ApplicationException.class)
    public void updateTest_null_id_exception() {
        IndustryBranchOperation operation = new IndustryBranchOperation();
        operation.setName("Foobar");

        try {
            operationService.update(null, operation);
        } catch (ApplicationException e) {
            assertEquals("java.lang.IllegalArgumentException", e.getRootCause().getExceptionName());
            assertEquals(ErrorCode.INTERNAL_ERROR, e.getErrorCode());
            throw e;
        }
    }

    @Test(expected = ApplicationException.class)
    public void updateTest_application_exception() {
        IndustryBranchOperation operation = new IndustryBranchOperation();
        operation.setName(TEST_OPERATION_NAME);

        ApplicationException ae = new ApplicationException();
        ae.setErrorCode(ErrorCode.RESOURCE_NOT_FOUND);

        when(operationRepository.getOne(TEST_OPERATION_ID)).thenThrow(ae);

        try {
            operationService.update(TEST_OPERATION_ID, operation);
        } catch (ApplicationException e) {
            assertEquals(ErrorCode.RESOURCE_NOT_FOUND, e.getErrorCode());
            throw e;
        }
    }

    @Test(expected = ApplicationException.class)
    public void updateTest_not_found() {
        IndustryBranchOperation operation = new IndustryBranchOperation();
        operation.setName(TEST_OPERATION_NAME);

        Long notFoundId = 77L;
        try {
            operationService.update(notFoundId, operation);
        } catch (ApplicationException e) {
            assertEquals(ErrorCode.RESOURCE_NOT_FOUND, e.getErrorCode());
            throw e;
        }
    }

    @Test
    public void deleteTest_success() {
        operationService.delete(TEST_OPERATION_ID);
    }

    @Test(expected = ApplicationException.class)
    public void deleteTest_not_found() {
        IndustryBranchOperation operation = new IndustryBranchOperation();
        operation.setName(TEST_OPERATION_NAME);

        Long notFoundId = 77L;
        try {
            operationService.delete(notFoundId);
        } catch (ApplicationException e) {
            assertEquals(ErrorCode.RESOURCE_NOT_FOUND, e.getErrorCode());
            throw e;
        }
    }

    @Test(expected = ApplicationException.class)
    public void deleteTest_null_id_exception() {
        try {
            operationService.delete(null);
        } catch (ApplicationException e) {
            assertEquals("java.lang.IllegalArgumentException", e.getRootCause().getExceptionName());
            assertEquals(ErrorCode.INTERNAL_ERROR, e.getErrorCode());
            throw e;
        }
    }

}
