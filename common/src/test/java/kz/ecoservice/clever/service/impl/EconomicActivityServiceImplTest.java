package kz.ecoservice.clever.service.impl;

import kz.ecoservice.clever.model.ErrorCode;
import kz.ecoservice.clever.model.entity.EconomicActivity;
import kz.ecoservice.clever.model.exception.ApplicationException;
import kz.ecoservice.clever.repository.EconomicActivityRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static org.junit.Assert.*;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class EconomicActivityServiceImplTest {

    private static final Long TEST_ID = 69L;
    private static final String TEST_NAME = "TEST_NAME";
    private static final String TEST_CODE = "TEST_CODE";

    @InjectMocks
    private EconomicActivityServiceImpl economicActivityService;

    @Mock
    private EconomicActivityRepository economicActivityRepository;

    @Before
    public void setUp() {
        EconomicActivity economicActivity = new EconomicActivity();
        economicActivity.setId(TEST_ID);
        economicActivity.setName(TEST_NAME);
        economicActivity.setCode(TEST_CODE);

        when(economicActivityRepository.getOne(TEST_ID)).thenReturn(economicActivity);
        when(economicActivityRepository.findById(TEST_ID)).thenReturn(Optional.of(economicActivity));
    }

    @Test
    public void findOneTest_success() {
        EconomicActivity economicActivity = economicActivityService.findOne(TEST_ID);
        assertNotNull(economicActivity);
        assertNotNull(economicActivity.getId());
        assertNotNull(economicActivity.getCode());
    }

    @Test(expected = ApplicationException.class)
    public void findOneTest_notFound() {
        Long notFoundId = 77L;
        try {
            economicActivityService.findOne(notFoundId);
        } catch (ApplicationException ae) {
            assertEquals(ErrorCode.RESOURCE_NOT_FOUND, ae.getErrorCode());
            throw ae;
        }
    }

    @Test(expected = ApplicationException.class)
    public void findOneTest_unexpected_exception() {
        try {
            economicActivityService.findOne(null);
        } catch (ApplicationException ae) {
            assertEquals(ErrorCode.INTERNAL_ERROR, ae.getErrorCode());
            throw ae;
        }
    }

    @Test
    public void findAllTest_success() {
        when(economicActivityRepository.findAll()).thenReturn(Collections.singletonList(new EconomicActivity()));
        List<EconomicActivity> economicActivities1 = economicActivityService.findAll();
        assertFalse(economicActivities1.isEmpty());

        when(economicActivityRepository.findAll()).thenReturn(Collections.emptyList());
        List<EconomicActivity> economicActivities2 = economicActivityService.findAll();
        assertTrue(economicActivities2.isEmpty());

        assertNotNull(economicActivities1);
        assertNotNull(economicActivities2);
    }

    @Test(expected = ApplicationException.class)
    public void findAllTest_application_exception() {
        ApplicationException ae = new ApplicationException();
        ae.setErrorCode(ErrorCode.RESOURCE_NOT_FOUND);
        when(economicActivityRepository.findAll()).thenThrow(ae);

        try {
            economicActivityService.findAll();
        } catch (ApplicationException e) {
            assertEquals(ErrorCode.RESOURCE_NOT_FOUND, e.getErrorCode());
            throw e;
        }
    }

    @Test(expected = ApplicationException.class)
    public void findAllTest_unexpected_exception() {
        when(economicActivityRepository.findAll()).thenThrow(new IllegalArgumentException("Illegal"));
        try {
            economicActivityService.findAll();
        } catch (ApplicationException e) {
            assertEquals(ErrorCode.INTERNAL_ERROR, e.getErrorCode());
            assertEquals(e.getMessage(), "Illegal");
            throw e;
        }
    }

    @Test
    public void saveTest_success() {
        EconomicActivity economicActivity = new EconomicActivity();
        economicActivity.setName(TEST_NAME);
        economicActivity.setCode(TEST_CODE);
        economicActivityService.save(economicActivity);
    }

    @Test(expected = ApplicationException.class)
    public void saveTest_unexpected_exception() {
        EconomicActivity economicActivity = new EconomicActivity();
        try {
            economicActivityService.save(economicActivity);
        } catch (ApplicationException e) {
            assertEquals("java.lang.IllegalArgumentException", e.getRootCause().getExceptionName());
            assertEquals(ErrorCode.INTERNAL_ERROR, e.getErrorCode());
            throw e;
        }
    }

    @Test(expected = ApplicationException.class)
    public void saveTest_application_exception() {
        EconomicActivity economicActivity = new EconomicActivity();
        economicActivity.setName(TEST_NAME);
        economicActivity.setCode(TEST_CODE);

        ApplicationException ae = new ApplicationException();
        ae.setErrorCode(ErrorCode.RESOURCE_NOT_FOUND);

        when(economicActivityRepository.save(economicActivity)).thenThrow(ae);

        try {
            economicActivityService.save(economicActivity);
        } catch (ApplicationException e) {
            assertEquals(ErrorCode.RESOURCE_NOT_FOUND, e.getErrorCode());
            throw e;
        }
    }

    @Test
    public void updateTest_success() {
        EconomicActivity economicActivity = new EconomicActivity();
        economicActivity.setName(TEST_NAME);
        economicActivity.setCode(TEST_CODE);
        economicActivityService.update(TEST_ID, economicActivity);
    }

    @Test(expected = ApplicationException.class)
    public void updateTest_null_id_and_object() {
        try {
            economicActivityService.update(null, null);
        } catch (ApplicationException e) {
            assertEquals("java.lang.IllegalArgumentException", e.getRootCause().getExceptionName());
            assertEquals(ErrorCode.INTERNAL_ERROR, e.getErrorCode());
            throw e;
        }
    }

    @Test(expected = ApplicationException.class)
    public void updateTest_null_object() {
        try {
            economicActivityService.update(TEST_ID, null);
        } catch (ApplicationException e) {
            assertEquals("java.lang.IllegalArgumentException", e.getRootCause().getExceptionName());
            assertEquals(ErrorCode.INTERNAL_ERROR, e.getErrorCode());
            throw e;
        }
    }

    @Test(expected = ApplicationException.class)
    public void updateTest_null_id_exception() {
        EconomicActivity economicActivity = new EconomicActivity();
        economicActivity.setName(TEST_NAME);
        economicActivity.setCode(TEST_CODE);

        try {
            economicActivityService.update(null, economicActivity);
        } catch (ApplicationException e) {
            assertEquals("java.lang.IllegalArgumentException", e.getRootCause().getExceptionName());
            assertEquals(ErrorCode.INTERNAL_ERROR, e.getErrorCode());
            throw e;
        }
    }

    @Test(expected = ApplicationException.class)
    public void updateTest_application_exception() {
        EconomicActivity economicActivity = new EconomicActivity();
        economicActivity.setName(TEST_NAME);
        economicActivity.setCode(TEST_CODE);

        ApplicationException ae = new ApplicationException();
        ae.setErrorCode(ErrorCode.RESOURCE_NOT_FOUND);

        when(economicActivityRepository.getOne(TEST_ID)).thenThrow(ae);

        try {
            economicActivityService.update(TEST_ID, economicActivity);
        } catch (ApplicationException e) {
            assertEquals(ErrorCode.RESOURCE_NOT_FOUND, e.getErrorCode());
            throw e;
        }
    }

    @Test(expected = ApplicationException.class)
    public void updateTest_not_found() {
        EconomicActivity economicActivity = new EconomicActivity();
        economicActivity.setName(TEST_NAME);
        economicActivity.setCode(TEST_CODE);

        Long notFoundId = 77L;
        try {
            economicActivityService.update(notFoundId, economicActivity);
        } catch (ApplicationException e) {
            assertEquals(ErrorCode.RESOURCE_NOT_FOUND, e.getErrorCode());
            throw e;
        }
    }

    @Test
    public void deleteTest_success() {
        economicActivityService.delete(TEST_ID);
    }

    @Test(expected = ApplicationException.class)
    public void deleteTest_not_found() {
        EconomicActivity economicActivity = new EconomicActivity();
        economicActivity.setName(TEST_NAME);
        economicActivity.setCode(TEST_CODE);

        Long notFoundId = 77L;
        try {
            economicActivityService.delete(notFoundId);
        } catch (ApplicationException e) {
            assertEquals(ErrorCode.RESOURCE_NOT_FOUND, e.getErrorCode());
            throw e;
        }
    }

    @Test(expected = ApplicationException.class)
    public void deleteTest_null_id_exception() {
        try {
            economicActivityService.delete(null);
        } catch (ApplicationException e) {
            assertEquals("java.lang.IllegalArgumentException", e.getRootCause().getExceptionName());
            assertEquals(ErrorCode.INTERNAL_ERROR, e.getErrorCode());
            throw e;
        }
    }

}
