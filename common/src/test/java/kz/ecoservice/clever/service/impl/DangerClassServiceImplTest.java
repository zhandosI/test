package kz.ecoservice.clever.service.impl;

import kz.ecoservice.clever.model.ErrorCode;
import kz.ecoservice.clever.model.entity.DangerClass;
import kz.ecoservice.clever.model.exception.ApplicationException;
import kz.ecoservice.clever.repository.DangerClassRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static org.junit.Assert.*;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class DangerClassServiceImplTest {

    private static final Long TEST_ID = 69L;
    private static final String TEST_NAME = "TEST_NAME";
    private static final Integer TEST_CODE = 05;

    @InjectMocks
    private DangerClassServiceImpl dangerClassService;

    @Mock
    private DangerClassRepository dangerClassRepository;

    @Before
    public void setUp() {
        DangerClass dangerClass = new DangerClass();
        dangerClass.setId(TEST_ID);
        dangerClass.setName(TEST_NAME);
        dangerClass.setCode(TEST_CODE);

        when(dangerClassRepository.getOne(TEST_ID)).thenReturn(dangerClass);
        when(dangerClassRepository.findById(TEST_ID)).thenReturn(Optional.of(dangerClass));
    }

    @Test
    public void findOneTest_success() {
        DangerClass dangerClass = dangerClassService.findOne(TEST_ID);
        assertNotNull(dangerClass);
        assertNotNull(dangerClass.getId());
        assertNotNull(dangerClass.getCode());
    }

    @Test(expected = ApplicationException.class)
    public void findOneTest_notFound() {
        Long notFoundId = 77L;
        try {
            dangerClassService.findOne(notFoundId);
        } catch (ApplicationException ae) {
            assertEquals(ErrorCode.RESOURCE_NOT_FOUND, ae.getErrorCode());
            throw ae;
        }
    }

    @Test(expected = ApplicationException.class)
    public void findOneTest_unexpected_exception() {
        try {
            dangerClassService.findOne(null);
        } catch (ApplicationException ae) {
            assertEquals(ErrorCode.INTERNAL_ERROR, ae.getErrorCode());
            throw ae;
        }
    }

    @Test
    public void findAllTest_success() {
        when(dangerClassRepository.findAll()).thenReturn(Collections.singletonList(new DangerClass()));
        List<DangerClass> dangerClasses1 = dangerClassService.findAll();
        assertFalse(dangerClasses1.isEmpty());

        when(dangerClassRepository.findAll()).thenReturn(Collections.emptyList());
        List<DangerClass> dangerClasses2 = dangerClassService.findAll();
        assertTrue(dangerClasses2.isEmpty());

        assertNotNull(dangerClasses1);
        assertNotNull(dangerClasses2);
    }

    @Test(expected = ApplicationException.class)
    public void findAllTest_application_exception() {
        ApplicationException ae = new ApplicationException();
        ae.setErrorCode(ErrorCode.RESOURCE_NOT_FOUND);
        when(dangerClassRepository.findAll()).thenThrow(ae);

        try {
            dangerClassService.findAll();
        } catch (ApplicationException e) {
            assertEquals(ErrorCode.RESOURCE_NOT_FOUND, e.getErrorCode());
            throw e;
        }
    }

    @Test(expected = ApplicationException.class)
    public void findAllTest_unexpected_exception() {
        when(dangerClassRepository.findAll()).thenThrow(new IllegalArgumentException("Illegal"));
        try {
            dangerClassService.findAll();
        } catch (ApplicationException e) {
            assertEquals(ErrorCode.INTERNAL_ERROR, e.getErrorCode());
            assertEquals(e.getMessage(), "Illegal");
            throw e;
        }
    }

    @Test
    public void saveTest_success() {
        DangerClass dangerClass = new DangerClass();
        dangerClass.setName(TEST_NAME);
        dangerClass.setCode(TEST_CODE);
        dangerClassService.save(dangerClass);
    }

    @Test(expected = ApplicationException.class)
    public void saveTest_unexpected_exception() {
        DangerClass dangerClass = new DangerClass();
        try {
            dangerClassService.save(dangerClass);
        } catch (ApplicationException e) {
            assertEquals("java.lang.IllegalArgumentException", e.getRootCause().getExceptionName());
            assertEquals(ErrorCode.INTERNAL_ERROR, e.getErrorCode());
            throw e;
        }
    }

    @Test(expected = ApplicationException.class)
    public void saveTest_application_exception() {
        DangerClass dangerClass = new DangerClass();
        dangerClass.setName(TEST_NAME);
        dangerClass.setCode(TEST_CODE);

        ApplicationException ae = new ApplicationException();
        ae.setErrorCode(ErrorCode.RESOURCE_NOT_FOUND);

        when(dangerClassRepository.save(dangerClass)).thenThrow(ae);

        try {
            dangerClassService.save(dangerClass);
        } catch (ApplicationException e) {
            assertEquals(ErrorCode.RESOURCE_NOT_FOUND, e.getErrorCode());
            throw e;
        }
    }

    @Test
    public void updateTest_success() {
        DangerClass dangerClass = new DangerClass();
        dangerClass.setName(TEST_NAME);
        dangerClass.setCode(TEST_CODE);
        dangerClassService.update(TEST_ID, dangerClass);
    }

    @Test(expected = ApplicationException.class)
    public void updateTest_null_id_and_object() {
        try {
            dangerClassService.update(null, null);
        } catch (ApplicationException e) {
            assertEquals("java.lang.IllegalArgumentException", e.getRootCause().getExceptionName());
            assertEquals(ErrorCode.INTERNAL_ERROR, e.getErrorCode());
            throw e;
        }
    }

    @Test(expected = ApplicationException.class)
    public void updateTest_null_object() {
        try {
            dangerClassService.update(TEST_ID, null);
        } catch (ApplicationException e) {
            assertEquals("java.lang.IllegalArgumentException", e.getRootCause().getExceptionName());
            assertEquals(ErrorCode.INTERNAL_ERROR, e.getErrorCode());
            throw e;
        }
    }

    @Test(expected = ApplicationException.class)
    public void updateTest_null_id_exception() {
        DangerClass dangerClass = new DangerClass();
        dangerClass.setName(TEST_NAME);
        dangerClass.setCode(TEST_CODE);

        try {
            dangerClassService.update(null, dangerClass);
        } catch (ApplicationException e) {
            assertEquals("java.lang.IllegalArgumentException", e.getRootCause().getExceptionName());
            assertEquals(ErrorCode.INTERNAL_ERROR, e.getErrorCode());
            throw e;
        }
    }

    @Test(expected = ApplicationException.class)
    public void updateTest_application_exception() {
        DangerClass dangerClass = new DangerClass();
        dangerClass.setName(TEST_NAME);
        dangerClass.setCode(TEST_CODE);

        ApplicationException ae = new ApplicationException();
        ae.setErrorCode(ErrorCode.RESOURCE_NOT_FOUND);

        when(dangerClassRepository.getOne(TEST_ID)).thenThrow(ae);

        try {
            dangerClassService.update(TEST_ID, dangerClass);
        } catch (ApplicationException e) {
            assertEquals(ErrorCode.RESOURCE_NOT_FOUND, e.getErrorCode());
            throw e;
        }
    }

    @Test(expected = ApplicationException.class)
    public void updateTest_not_found() {
        DangerClass dangerClass = new DangerClass();
        dangerClass.setName(TEST_NAME);
        dangerClass.setCode(TEST_CODE);

        Long notFoundId = 77L;
        try {
            dangerClassService.update(notFoundId, dangerClass);
        } catch (ApplicationException e) {
            assertEquals(ErrorCode.RESOURCE_NOT_FOUND, e.getErrorCode());
            throw e;
        }
    }

    @Test
    public void deleteTest_success() {
        dangerClassService.delete(TEST_ID);
    }

    @Test(expected = ApplicationException.class)
    public void deleteTest_not_found() {
        DangerClass dangerClass = new DangerClass();
        dangerClass.setName(TEST_NAME);
        dangerClass.setCode(TEST_CODE);

        Long notFoundId = 77L;
        try {
            dangerClassService.delete(notFoundId);
        } catch (ApplicationException e) {
            assertEquals(ErrorCode.RESOURCE_NOT_FOUND, e.getErrorCode());
            throw e;
        }
    }

    @Test(expected = ApplicationException.class)
    public void deleteTest_null_id_exception() {
        try {
            dangerClassService.delete(null);
        } catch (ApplicationException e) {
            assertEquals("java.lang.IllegalArgumentException", e.getRootCause().getExceptionName());
            assertEquals(ErrorCode.INTERNAL_ERROR, e.getErrorCode());
            throw e;
        }
    }

}
