package kz.ecoservice.clever.controller;

import kz.ecoservice.clever.model.dto.response.ResponseErrorDto;
import kz.ecoservice.clever.model.dto.response.ResponseSuccessDto;
import kz.ecoservice.clever.model.exception.ApplicationException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.Collections;
import java.util.List;


public abstract class BaseController {

    private static final String SUCCESS = "success";
    private static final String ERROR = "error";

    protected ResponseSuccessBuilder success() {
        return new ResponseSuccessBuilder(SUCCESS);
    }

    protected ResponseErrorBuilder error() {
        return new ResponseErrorBuilder(ERROR);
    }

    protected static class ResponseSuccessBuilder {

        private static final List EMPTY_LIST_BODY = Collections.emptyList();

        private String resStatus;
        private HttpStatus httpStatus;
        private HttpHeaders headers;
        private Object body;

        /**
         * No access to create builder object outside @link {BaseController}
         * @param resStatus - {"success", "error}
         */
        private ResponseSuccessBuilder(String resStatus) {
            this.resStatus = resStatus;
        }

        public ResponseSuccessBuilder withStatus(HttpStatus status) {
            this.httpStatus = status;
            return this;
        }

        public ResponseSuccessBuilder withHeaders(HttpHeaders headers) {
            this.headers = headers;
            return this;
        }

        public ResponseSuccessBuilder withBody() {
            this.body = EMPTY_LIST_BODY;
            return this;
        }

        public ResponseSuccessBuilder withBody(Object body) {
            this.body = body;
            return this;
        }

        public ResponseEntity<?> build() {
            if (httpStatus == null)
                httpStatus = HttpStatus.OK;

            return ResponseEntity
                    .status(httpStatus)
                    .headers(headers)
                    .body(new ResponseSuccessDto(resStatus, body));
        }
    }


    protected static class ResponseErrorBuilder {

        private String resStatus;
        private HttpStatus httpStatus;
        private HttpHeaders headers;
        private ApplicationException exception;

        /**
         * No access to create builder object outside @link {BaseController}
         * @param resStatus - {"success", "error"}
         */
        private ResponseErrorBuilder(String resStatus) {
            this.resStatus = resStatus;
        }

        public ResponseErrorBuilder withStatus(HttpStatus status) {
            this.httpStatus = status;
            return this;
        }

        public ResponseErrorBuilder withHeaders(HttpHeaders headers) {
            this.headers = headers;
            return this;
        }

        public ResponseErrorBuilder withException(ApplicationException e) {
            this.exception = e;
            return this;
        }

        public ResponseEntity<?> build() {
            if (httpStatus == null)
                httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;

            ResponseErrorDto errorDto;

            if (exception == null)
                errorDto = new ResponseErrorDto(resStatus, "unknown internal error");
            else if (exception.getRootCause() == null)
                errorDto = new ResponseErrorDto(
                        resStatus,
                        exception.getErrorCode().toString(),
                        exception.getMessage());
            else {
                ResponseErrorDto rootErrorDto = new ResponseErrorDto(
                        exception.getRootCause().getMessage(),
                        exception.getRootCause().getExceptionName(),
                        exception.getRootCause().getClassName(),
                        exception.getRootCause().getMethodName(),
                        exception.getRootCause().getLineNumber());

                errorDto = new ResponseErrorDto(
                        resStatus,
                        exception.getErrorCode().toString(),
                        exception.getMessage(),
                        rootErrorDto);
            }

            return ResponseEntity
                    .status(httpStatus)
                    .headers(headers)
                    .body(errorDto);
        }
    }
}
