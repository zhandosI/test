package kz.ecoservice.clever.util.mapper;

import kz.ecoservice.clever.model.entity.BaseEntity;
import org.modelmapper.ModelMapper;

import java.util.List;
import java.util.stream.Collectors;


public abstract class AbstractEntityDtoMapper<E extends BaseEntity, D> implements EntityDtoMapper<E, D> {

    private final Class<E> entityClass;
    private final Class<D> dtoClass;

    private static ModelMapper defaultMapper;

    public AbstractEntityDtoMapper(Class<E> entityClass, Class<D> dtoClass) {
        this.entityClass = entityClass;
        this.dtoClass = dtoClass;
    }

    public D toDto(E entity) {
        return mapper().map(entity, dtoClass);
    }

    public E toEntity(D dto) {
        return mapper().map(dto, entityClass);
    }

    public List<D> toDto(List<E> entities) {
        return entities.stream()
                .map(this::toDto)
                .collect(Collectors.toList());
    }

    public List<E> toEntity(List<D> dtos) {
        return dtos.stream()
                .map(this::toEntity)
                .collect(Collectors.toList());
    }

    /**
     * Could be overriden to use custom configuration for ModelMapper
     * @return an instance of default ModelMapper
     */
    protected synchronized ModelMapper mapper() {
        if (defaultMapper == null) {
            defaultMapper = new ModelMapper();
        }
        return defaultMapper;
    }

}
