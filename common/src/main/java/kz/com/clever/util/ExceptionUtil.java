package kz.ecoservice.clever.util;


public final class ExceptionUtil {

    private ExceptionUtil() {}

    public static Throwable getRootCause(Throwable th) {
        if (th == null)
            return null;

        Throwable cause;
        Throwable res = th;

        while ((cause = res.getCause()) != null && res != cause)
            res = cause;

        return res;
    }

    public static StackTraceElement getPackageRootStackElement(Throwable th, String packageName) {
        if (th == null || th.getStackTrace() == null || packageName == null)
            return null;

        String className;
        for (StackTraceElement ste: th.getStackTrace()) {
            className = ste.getClassName();

            if (className.startsWith(packageName)) {
                return ste;
            }
        }

        return null;
    }

}
