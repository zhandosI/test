package kz.ecoservice.clever.util.mapper.impl;

import kz.ecoservice.clever.model.dto.RoleDto;
import kz.ecoservice.clever.model.entity.Role;
import kz.ecoservice.clever.util.mapper.AbstractEntityDtoMapper;
import org.springframework.stereotype.Component;

@Component
public class RoleMapper extends AbstractEntityDtoMapper<Role, RoleDto> {

    public RoleMapper() { super(Role.class, RoleDto.class);
    }
}
