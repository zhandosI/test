package kz.ecoservice.clever.util.mapper.impl;

import kz.ecoservice.clever.model.dto.UserDto;
import kz.ecoservice.clever.model.entity.User;
import kz.ecoservice.clever.util.mapper.AbstractEntityDtoMapper;
import org.springframework.stereotype.Component;

@Component
public class UserMapper extends AbstractEntityDtoMapper<User, UserDto> {

    public UserMapper() {
        super(User.class, UserDto.class);
    }

}
