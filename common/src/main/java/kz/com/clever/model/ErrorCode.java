package kz.ecoservice.clever.model;

public enum ErrorCode {
    INTERNAL_ERROR,
    AUTH_ERROR,
    RESOURCE_NOT_FOUND,
    UNIQUE_RESOURCE_CONFLICT,

}
