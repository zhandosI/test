package kz.ecoservice.clever.model.exception.builder;

import kz.ecoservice.clever.model.ErrorCode;

public class AuthExceptionBuilder extends ExceptionBuilder {

    private String message;

    public AuthExceptionBuilder(String message) {
        this.message = message;
    }

    @Override
    ExceptionBuilder withMessage() {
        exception.setMessage(message);
        return this;
    }

    @Override
    ExceptionBuilder withErrorCode() {
        exception.setErrorCode(ErrorCode.AUTH_ERROR);
        return this;
    }

    @Override
    ExceptionBuilder withRootCause() {
        // No cause needed
        return this;
    }

}
