package kz.ecoservice.clever.model.exception.builder;

import kz.ecoservice.clever.model.ErrorCode;
import kz.ecoservice.clever.model.exception.ApplicationExceptionDetail;
import kz.ecoservice.clever.util.ExceptionUtil;

public class ApplicationExceptionBuilder extends ExceptionBuilder {

    private Throwable cause;

    public ApplicationExceptionBuilder(Throwable cause) {
        this.cause = cause;
    }

    @Override
    ExceptionBuilder withMessage() {
        if (this.cause != null)
            exception.setMessage(cause.getMessage());
        return this;
    }

    @Override
    ExceptionBuilder withErrorCode() {
        exception.setErrorCode(ErrorCode.INTERNAL_ERROR);
        return this;
    }

    @Override
    ExceptionBuilder withRootCause() {
        Throwable rootCause = ExceptionUtil.getRootCause(this.cause);
        StackTraceElement rootSte = ExceptionUtil.getPackageRootStackElement(this.cause, "kz.ecoservice.clever");

        if (rootCause != null) {
            ApplicationExceptionDetail axe = new ApplicationExceptionDetail(
                    rootCause.getClass().getName(), rootCause.getMessage());

            if (rootSte != null) {
                if (rootSte.getClassName() != null)
                    axe.setClassName(rootSte.getClassName());

                if (rootSte.getMethodName() != null)
                    axe.setMethodName(rootSte.getMethodName());

                if (rootSte.getLineNumber() != 0)
                    axe.setLineNumber(rootSte.getLineNumber());
            }

            exception.setRootCause(axe);
        }

        return this;
    }

}
