package kz.ecoservice.clever.model.enm;

public enum SessionCode {
    ACTIVE,
    EXPIRED,
    INVALIDATED
}
