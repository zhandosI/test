package kz.ecoservice.clever.model.enm;

public enum Status {
    ACTIVE, NOT_ACTIVE, DELETED
}
