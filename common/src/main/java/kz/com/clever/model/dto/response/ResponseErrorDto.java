package kz.ecoservice.clever.model.dto.response;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;


@Data
@EqualsAndHashCode(callSuper = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ResponseErrorDto extends ResponseDto {

    @JsonProperty(value = "code")
    private String code;

    @JsonProperty(value = "message")
    private String message;

    @JsonProperty(value = "exceptionName")
    private String exceptionName;

    @JsonProperty(value = "className")
    private String className;

    @JsonProperty(value = "methodName")
    private String methodName;

    @JsonProperty(value = "lineNumber")
    private Integer lineNumber;

    @JsonProperty(value = "cause")
    private ResponseErrorDto cause;

    public ResponseErrorDto(String status, String message) {
        super(status);
        this.message = message;
    }

    public ResponseErrorDto(String status, String code, String message) {
        super(status);
        this.code = code;
        this.message = message;
    }

    public ResponseErrorDto(String message, String exceptionName, String className, String methodName, Integer lineNumber) {
        this.message = message;
        this.exceptionName = exceptionName;
        this.className = className;
        this.methodName = methodName;
        this.lineNumber = lineNumber;
    }

    public ResponseErrorDto(String status, String code, String message, ResponseErrorDto cause) {
        super(status);
        this.code = code;
        this.message = message;
        this.cause = cause;
    }

}
