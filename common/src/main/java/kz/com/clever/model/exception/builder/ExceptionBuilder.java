package kz.ecoservice.clever.model.exception.builder;

import kz.ecoservice.clever.model.exception.ApplicationException;


abstract class ExceptionBuilder {

    ApplicationException exception;

    abstract ExceptionBuilder withMessage();
    abstract ExceptionBuilder withErrorCode();
    abstract ExceptionBuilder withRootCause();

    ExceptionBuilder createException() {
        exception = new ApplicationException();
        return this;
    }

    ApplicationException build() {
        return exception;
    }

}
