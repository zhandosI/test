package kz.ecoservice.clever.model.enm;

public enum  DataType {
    INT, DOUBLE, STRING, BOOLEAN, LONG, DATE
}
