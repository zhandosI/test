package kz.ecoservice.clever.model.exception;

import lombok.Data;

@Data
public class ApplicationExceptionDetail {

    private String message;
    private String exceptionName;
    private String className;
    private String methodName;
    private Integer lineNumber;

    public ApplicationExceptionDetail(String exceptionName, String message) {
        this.exceptionName = exceptionName;
        this.message = message;
    }

}
