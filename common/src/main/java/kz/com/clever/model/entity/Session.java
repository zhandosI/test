package kz.ecoservice.clever.model.entity;

import lombok.Data;
import lombok.EqualsAndHashCode;
import org.hibernate.annotations.Where;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "clever_sessions")
@Where(clause = "status = 'ACTIVE'")
@Data
@EqualsAndHashCode(callSuper = true)
public class Session extends BaseEntity {

    @Column(name = "access_token", nullable = false)
    private String accessToken;

    @Column(name = "duration", nullable = false)
    private Long duration;

    @Column(name = "ended_at", nullable = false)
    private Date endedDate;

    @ManyToOne
    @JoinColumn(name = "user_id")
    private User user;


}
