package kz.ecoservice.clever.model.dto.response;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ResponseSuccessDto extends ResponseDto {

    @JsonProperty(value = "data")
    private Object data;

    public ResponseSuccessDto(String status, Object data) {
        super(status);
        this.data = data;
    }

}
