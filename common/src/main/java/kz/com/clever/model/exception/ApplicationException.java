package kz.ecoservice.clever.model.exception;

import lombok.Data;
import lombok.EqualsAndHashCode;


@Data
@EqualsAndHashCode(callSuper = true)
public class ApplicationException extends RuntimeException {

    private String message;
    private Enum errorCode;
    private ApplicationExceptionDetail rootCause;

}
