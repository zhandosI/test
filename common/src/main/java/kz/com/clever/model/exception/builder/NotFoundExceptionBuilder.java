package kz.ecoservice.clever.model.exception.builder;

import kz.ecoservice.clever.model.ErrorCode;

public class NotFoundExceptionBuilder extends ExceptionBuilder {

    private String message;

    public NotFoundExceptionBuilder(String message) {
        this.message = message;
    }

    @Override
    ExceptionBuilder withMessage() {
        exception.setMessage(message);
        return this;
    }

    @Override
    ExceptionBuilder withErrorCode() {
        exception.setErrorCode(ErrorCode.RESOURCE_NOT_FOUND);
        return this;
    }

    @Override
    ExceptionBuilder withRootCause() {
        return this;
    }

}
