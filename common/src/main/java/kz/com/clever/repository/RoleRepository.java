package kz.ecoservice.clever.repository;

import kz.ecoservice.clever.model.entity.Role;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RoleRepository extends JpaRepository<Role, Long> {
}
