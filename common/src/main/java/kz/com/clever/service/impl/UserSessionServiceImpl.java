package kz.ecoservice.clever.service.impl;

import kz.ecoservice.clever.model.entity.Session;
import kz.ecoservice.clever.model.entity.User;
import kz.ecoservice.clever.service.UserSessionService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

/**
 * @author Zhandos Irzhanov
 */

@Service
@Slf4j
public class UserSessionServiceImpl implements UserSessionService {

    @Override
    public void save(Session session) {

    }

    @Override
    public Session findByUser(User user) {
        return null;
    }

    @Override
    public void delete(Long id) {

    }
}
