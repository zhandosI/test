package kz.ecoservice.clever.service;


import kz.ecoservice.clever.model.entity.Session;
import kz.ecoservice.clever.model.entity.User;

public interface UserSessionService {
    void save(Session session);
    Session findByUser(User user);
    void delete(Long id);
}
