package kz.ecoservice.clever.service;

import kz.ecoservice.clever.model.entity.Role;

import java.util.List;

public interface RoleService {

    List<Role> findAll();
    Role save(Role user);
}
