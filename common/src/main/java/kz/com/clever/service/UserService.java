package kz.ecoservice.clever.service;

import kz.ecoservice.clever.model.entity.User;

import java.util.List;

public interface UserService {

    User findOne(Long id);
    User findOne(String username);
    List<User> findAll();
    User save(User user);

}
