package kz.ecoservice.clever.service.impl;


import kz.ecoservice.clever.model.entity.User;
import kz.ecoservice.clever.model.exception.ApplicationException;
import kz.ecoservice.clever.model.exception.builder.ApplicationExceptionBuilder;
import kz.ecoservice.clever.model.exception.builder.ExceptionDirector;
import kz.ecoservice.clever.repository.UserRepository;
import kz.ecoservice.clever.service.UserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.security.crypto.password.PasswordEncoder;
import java.util.List;

@Service
@Slf4j
public class UserServiceImpl implements UserService {

    private  UserRepository userRepository;
    private  PasswordEncoder passwordEncoder;

    @Autowired
    public void setUserRepository(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Autowired
    public void setPasswordEncoder(PasswordEncoder passwordEncoder) {
        this.passwordEncoder = passwordEncoder;
    }

    @Override
    public User findOne(Long id) {
        try {
            return userRepository.findById(id).orElse(null);
        } catch (ApplicationException e) {
            throw e;
        } catch (Exception e) {
            log.error("IN findOne: {}", e.getMessage(), e);
            throw new ExceptionDirector()
                    .applyBuilder(new ApplicationExceptionBuilder(e))
                    .build();
        }
    }

    @Override
    public User findOne(String username) {
        try {
            return userRepository.findByUsername(username);
        } catch (ApplicationException e) {
            throw e;
        } catch (Exception e) {
            log.error("IN findOne: {}", e.getMessage(), e);
            throw new ExceptionDirector()
                    .applyBuilder(new ApplicationExceptionBuilder(e))
                    .build();
        }
    }

    @Override
    public List<User> findAll() {
        try {
            return userRepository.findAll();
        } catch (ApplicationException e) {
            throw e;
        } catch (Exception e) {
            log.error("IN findAll: {}", e.getMessage(), e);
            throw new ExceptionDirector()
                    .applyBuilder(new ApplicationExceptionBuilder(e))
                    .build();
        }
    }

    @Override
    public User save(User user) {
        try {
            if (user.getPassword() != null) {
                user.setPassword(passwordEncoder.encode(user.getPassword()));
            }

            User newUser = userRepository.save(user);


            return newUser;

        } catch (ApplicationException e) {
            throw e;
        } catch (Exception e) {
            log.error("IN save User: {}", e.getMessage(), e);
            throw new ExceptionDirector()
                    .applyBuilder(new ApplicationExceptionBuilder(e))
                    .build();
        }
    }

}
