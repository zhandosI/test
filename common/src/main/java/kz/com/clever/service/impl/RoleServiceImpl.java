package kz.ecoservice.clever.service.impl;

import kz.ecoservice.clever.model.entity.Role;
import kz.ecoservice.clever.model.exception.ApplicationException;
import kz.ecoservice.clever.model.exception.builder.ApplicationExceptionBuilder;
import kz.ecoservice.clever.model.exception.builder.ExceptionDirector;
import kz.ecoservice.clever.repository.RoleRepository;
import kz.ecoservice.clever.service.RoleService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@Slf4j
public class RoleServiceImpl implements RoleService {

    private RoleRepository roleRepository;

    @Autowired
    public void setRoleRepository(RoleRepository roleRepository) {
        this.roleRepository = roleRepository;
    }

    @Override
    public List<Role> findAll() {
        try {
            return roleRepository.findAll();
        } catch (ApplicationException e) {
            throw e;
        } catch (Exception e) {
            log.error("IN findAll in Role: {}", e.getMessage(), e);
            throw new ExceptionDirector()
                    .applyBuilder(new ApplicationExceptionBuilder(e))
                    .build();
        }
    }

    @Override
    public Role save(Role role) {
        try {
            Role newRole = roleRepository.save(role);
            return newRole;
        } catch (ApplicationException e) {
            throw e;
        } catch (Exception e) {
            log.error("IN save Role: {}", e.getMessage(), e);
            throw new ExceptionDirector()
                    .applyBuilder(new ApplicationExceptionBuilder(e))
                    .build();
        }

    }
}
